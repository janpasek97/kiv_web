<?php

/**
 * Class loginController controller of the login page
 */
class loginController extends baseController
{
    /**
     * handles the actions connected to this page -> login, signup or logout and renders the page content
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        $content = array();
        $content["signupActive"] = null;
        $content["signinAlert"] = null;
        $content["signupAlert"] = null;
        $content["signupSuccessful"] = null;

        if(isset($_POST["action"])){
            $action = $_POST["action"];
            if($action == "login"){
                $username = $_POST["loginUsername"];
                $password = $_POST["loginPassword"];
                $result = $this->login->login($username, $password);
                if($result){
                    $newUrl = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser")));
                    header("Location: $newUrl");
                    die();
                }
                else {
                    $content["signinAlert"] = "You have entered wrong login details";
                }
            }
            else if($action == "logout"){
                $this->login->logout();
                $newUrl = $this->makeURL(array());
                header("Location: $newUrl");
                die();
            }
            else if($action == "signup"){
                $username = $_POST["signupUsername"];
                $email = $_REQUEST["signupEmail"];
                $password = $_REQUEST["signupPassword"];
                $result = $this->login->signup($username, $email, password_hash($password, PASSWORD_DEFAULT));
                if($result) {
                    $content["signupSuccessful"] = "Registration was successful. You can now log in.";
                }
                else {
                    $content["signupAlert"] = "User with this name or e-mail already exists.";
                    $content["signupActive"] = "true";
                }
            }
        }

        return $this->renderContent($content);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $params = array();
        $params["signupActive"] = $content["signupActive"];
        $params["signupAlert"] = $content["signupAlert"];
        $params["signinAlert"] = $content["signinAlert"];
        $params["signupSuccessful"] = $content["signupSuccessful"];
        $params["homeLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"home")));
        return $this->twig->render("login.twig", $params);
    }
}