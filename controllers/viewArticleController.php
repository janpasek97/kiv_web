<?php

/**
 * Class viewArticleController controller of the article view page
 */
class viewArticleController extends baseController
{
    /**
     * Renders the page content
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if(isset($_REQUEST["articleID"])){
            $articleID = $_REQUEST["articleID"];
            return $this->renderContent($articleID);
        }
        else {
            header("Location: index.php");
            die();
        }
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $articleResult = $this->article->getArticleByID($content);

        $article = array();
        $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
        $article["author"] = $articleResult[USER_NAME_COLUMN];
        $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];
        $article["description"] = $articleResult[ARTICLE_OVERVIEW_COLUMN];
        $article["text"] = $articleResult[ARTICLE_TEXT_COLUMN];
        $article["userIconPath"] = "img/user_icon.png";
        $article["file"] = $articleResult[ARTICLE_FILE];

        $categoriesResult = $this->article->getArticleCategories($content);
        $articleCategories = array();
        foreach ($categoriesResult as $categoryResult){
            $index = count($articleCategories);
            $articleCategories[$index] = $categoryResult[CATEGORY_NAME_COLUMN];
        }

        $article["categories"] = $articleCategories;
        return $this->twig->render("article.twig", array("article"=>$article));
    }
}