<?php

/**
 * Class errorController renders a page that is displayed when invalid page or subpage is requested
 */
class errorController extends baseController
{
    /**
     * Only returns rendered content
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        return $this->renderContent(null);
    }

    /**
     * Renders the error page
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $homepageLink = $this->makeURL(array(array("name"=>"page", "value"=>"home")));
        return $this->twig->render("error404.twig",array("homepageLink"=>$homepageLink));
    }
}