<?php

/**
 * Class loggedUserController controller of logged user page.
 */
class loggedUserController extends baseController
{
    /**
     * @var string subpage
     */
    private $subpage;

    /**
     * Evaluates the selected subpage and calls it controllers. Also renders the page content
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        $subpages = array("myArticles"=>"author",
            "viewReview"=>"reviewer",
            "createReview"=>"reviewer",
            "createArticle"=>"author",
            "myReviews"=>"reviewer",
            "usersAdministration"=>"administrator",
            "articlesAdministration"=>"administrator",
            "error"=>"Error 404",
            "accessError"=>"Access restricted error",
            "updateArticle" => "author");

        //get subpage
        if(isset($_REQUEST["subpage"])){
            $this->subpage = $_REQUEST["subpage"];
        }
        else {
            $this->subpage = $this->getDefaultSubpage();
        }

        if(!array_key_exists($this->subpage, $subpages)){
            $this->subpage = "error";
        }
        else if(!$this->login->hasUserRight($subpages[$this->subpage]))
        {
            $this->subpage = "accessError";
        }

        $controller = $this->subpage."Controller";
        include_once ("controllers/$controller.php");

        $$controller = new $controller($this->twig, $this->models);
        $content = $$controller->indexAction();

        return $this->renderContent($content);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $params = array();

        $params["loggedUser"] = $this->getLoggedUser();
        $params["navItems"] = $this->getNavItems();
        $params["homeLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"home")));
        $params["logoutLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"login")));
        $params["content"] = $content;
        return $this->twig->render("logged_user.twig", $params);
    }

    /**
     * Getter of default subpage according to logged user's rights
     * @return string defualt subpage name
     */
    private function getDefaultSubpage(){
        if($this->login->hasUserRight("author")){
            $subpage = "myArticles";
        }
        else if($this->login->hasUserRight("reviewer")){
            $subpage = "myReviews";
        }
        else if($this->login->hasUserRight("administrator")){
            $subpage = "articlesAdministration";
        }
        else {
            $subpage = "accessError";
        }

        return $subpage;
    }

    /**
     * Getter of logged user information
     * @return array formatted user information
     */
    private function getLoggedUser(){
        $loggedUser = array();
        $loggedUser["iconPath"] = "img/user_icon.png";
        $loggedUser["name"] = $this->login->getLoggedUserInformation()["username"];
        return $loggedUser;
    }

    /**
     * Creates the navigation items according to logged user's rights
     * @return array of formatted navigation items
     */
    private function getNavItems(){
        $navItems = array();
        if ($this->login->hasUserRight("author")) {
            $navItem = array();
            if ($this->subpage == "myArticles") {
                $navItem["active"] = "active";
            } else {
                $navItem["active"] = "";
            }
            $navItem["link"] = "index.php?page=loggedUser&subpage=myArticles";
            $navItem["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"myArticles")));
            $navItem["name"] = "My articles";
            $navItems[] = $navItem;
        }
        if ($this->login->hasUserRight("reviewer")) {
            $navItem = array();
            if ($this->subpage == "myReviews") {
                $navItem["active"] = "active";
            } else {
                $navItem["active"] = "";
            }
            $navItem["link"] = $this->makeURL(array(array("name" => "page", "value" => "loggedUser"), array("name" => "subpage", "value" => "myReviews")));
            $navItem["name"] = "My reviews";
            $navItems[] = $navItem;
        }
        if ($this->login->hasUserRight("administrator")) {
            $navItem = array();
            if ($this->subpage == "articlesAdministration") {
                $navItem["active"] = "active";
            } else {
                $navItem["active"] = "";
            }
            $navItem["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"articlesAdministration")));
            $navItem["name"] = "Articles administration";
            $navCount = count($navItems);
            $navItems[$navCount] = $navItem;
            $navItem = array();
            if ($this->subpage == "usersAdministration") {
                $navItem["active"] = "active";
            } else {
                $navItem["active"] = "";
            }
            $navItem["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"usersAdministration")));
            $navItem["name"] = "Users administration";
            $navItems[] = $navItem;
        }
        return $navItems;
    }
}