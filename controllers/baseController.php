<?php

class baseController
{
    /** @var Twig_Environment */
    protected $twig;

    protected $models;

    //models list
    /** @var UserLogin */
    protected $login;

    /** @var categoryModel */
    protected $category;

    /** @var articleModel */
    protected $article;

    /** @var userModel */
    protected $user;

    /** @var reviewModel */
    protected $review;

    /** @var userRightModel */
    protected $right;

    /**
     * baseController constructor.
     * @param $twig instance of twig to render templates
     * @param $models array of all models used for work with DB
     */
    public function __construct($twig, $models)
    {
        $this->models = $models;
        $this->twig = $twig;

        //assign models from given array
        if($models != null){
            foreach ($models as $modelName => $model){
                $this->$modelName = $model;
            }
        }
    }

    /**
     * Main handler of the controller
     * @return string content of the page
     */
    public function indexAction(){
        return "missing index method";
    }

    /**
     * Render the content of the controllers page
     */
    public function renderContent($content) {
        return "missing render method";
    }

    /**
     * Creates an URL to display page content
     *
     * @param $url_param get parameters of URL
     * @return string URL to this page
     */
    public function makeURL($url_param){
        $paramCount = count($url_param);
        $params = array();
        foreach ($url_param as $param) {
            $params[$param["name"]] = $param["value"];
        }

        if(!ENABLE_NICE_URL) {
            $link = "index.php";
            if (count($url_param) > 0) {
                $link .= "?";
                $index = 0;
                foreach ($url_param as $param) {
                    $index++;
                    $link .= $param["name"] . "=";
                    $link .= $param["value"];
                    if ($index < count($url_param)) {
                        $link .= "&";
                    }
                }
            }
            return $link;
        }
        else {
            if ($paramCount == 0) {
                return "home";
            }

            if ($paramCount == 1) {
                if (array_key_exists("page", $params)) {
                    if ($params["page"] == "home") {
                        return "home";
                    } else if ($params["page"] == "loggedUser") {
                        return "user";
                    } else if ($params["page"] == "login") {
                        return "login";
                    }
                }
            } else if ($paramCount == 2) {
                if (array_key_exists("page", $params)) {
                    if ($params["page"] == "home") {
                        if (array_key_exists("subpage", $params)) {
                            if ($params["subpage"] == "displayUsers") {
                                return "authors";
                            }
                        }
                    } else if ($params["page"] == "loggedUser") {
                        $link = "user/";
                        if (array_key_exists("subpage", $params)) {
                            $subpage = $params["subpage"];
                            if ($subpage == "myArticles") {
                                $link .= "myArticles";
                            } else if ($subpage == "createArticle") {
                                $link .= "createArticle";
                            } else if ($subpage == "myReviews") {
                                $link .= "myReviews";
                            } else if ($subpage == "articlesAdministration") {
                                $link .= "articlesAdministration";
                            } else if ($subpage == "usersAdministration") {
                                $link .= "usersAdministration";
                            }
                            return $link;
                        }
                    }
                }
            } else if ($paramCount == 3) {
                if (array_key_exists("page", $params)) {
                    $page = $params["page"];
                    if ($page == "home") {
                        if (array_key_exists("subpage", $params)) {
                            $subpage = $params["subpage"];
                            if ($subpage == "articles") {
                                if (array_key_exists("categoryID", $params)) {
                                    $link = "category/" . $params["categoryID"];
                                } else {
                                    $link = "home";
                                }

                                if (array_key_exists("pagination", $params)) {
                                    $link .= "/" . $params["pagination"];
                                }
                                return $link;
                            } else if ($subpage == "viewArticle") {
                                if (array_key_exists("articleID", $params)) {
                                    return "article/" . $params["articleID"];
                                }
                            }
                        }
                    } else if ($page == "loggedUser") {
                        if (array_key_exists("subpage", $params)) {
                            $subpage = $params["subpage"];
                            if ($subpage == "createReview") {
                                if (array_key_exists("articleID", $params)) {
                                    return "user/createReview/" . $params["articleID"];
                                }
                            } else if ($subpage == "viewReview") {
                                if (array_key_exists("reviewID", $params)) {
                                    return "user/viewReview/" . $params["reviewID"];
                                }
                            } else if($subpage == "updateArticle") {
                                if(array_key_exists("articleID", $params)){
                                    return "user/updateArticle/".$params["articleID"];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}