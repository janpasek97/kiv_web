<?php

/**
 * Class usersAdministrationController controller of usersAdministration page
 */
class usersAdministrationController extends baseController
{
    /**
     * Handles the possible update user action and calls page renderer
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if(isset($_POST["action"])){
            $action = $_POST["action"];
            if($action == "updateUser"){
                $this->updateUser();
            }
        }
        return $this->renderContent(null);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $usersQueryResult = $this->user->getAllUsers();

        $users = array();
        foreach ($usersQueryResult as $userResult) {
            $user = array();
            $userID = $userResult[USER_ID_COLUMN];
            $user["name"] = $userResult[USER_NAME_COLUMN];
            $user["email"] = $userResult[USER_EMAIL_COLUMN];
            $user["ID"] = $userID;
            //User rights
            $rights = $this->user->getUsersRights($userID);

            $user["author"] = "";
            $user["reviewer"] = "";
            $user["admin"] = "";

            foreach ($rights as $right) {
                if ($right[RIGHT_NAME_COLUMN] == "author") {
                    $user["author"] = "checked";
                }
                if ($right[RIGHT_NAME_COLUMN] == "administrator") {
                    $user["admin"] = "checked";
                }
                if ($right[RIGHT_NAME_COLUMN] == "reviewer") {
                    $user["reviewer"] = "checked";
                }
            }

            $users[] = $user;
        }

        $actionLink = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"usersAdministration")));

        return $this->twig->render("user_administration.twig", array("users" => $users, "actionLink"=>$actionLink));
    }

    /**
     * Updates the user rights in database
     */
    public function updateUser(){
        $allset = true;
        if(!isset($_REQUEST["rights"])){
            $allset = false;
        }
        if(!isset($_REQUEST["userID"])){
            $allset = false;
        }
        if($allset){
            $rights = $_REQUEST["rights"];
            $userID = $_REQUEST["userID"];

            $this->right->deleteAllUsersRights($userID);

            foreach ($rights as $right){
                $rightID = $this->right->getRightID($right);
                $this->right->assignRight($userID, $rightID);
            }
        }
    }
}