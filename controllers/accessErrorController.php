<?php

/**
 * Class accessErrorController handles a situation when user is trespassing
 */
class accessErrorController extends baseController
{
    /**
     * Only returns rendered content of the page
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */

    public function indexAction()
    {
        return $this->renderContent(null);
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $homepageLink = $this->makeURL(array(array("name"=>"page", "value"=>"home")));
        return $this->twig->render("accessError.twig", array("homepageLink"=>$homepageLink));
    }
}