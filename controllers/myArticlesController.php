<?php

/**
 * Class myArticlesController controller of myArticles page
 */
class myArticlesController extends baseController
{
    /**
     * Calls a method for rendering the page
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        return $this->renderContent(null);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $linkParam = array();
        $linkParam[] = array("name"=>"page", "value"=>"loggedUser");
        $linkParam[] = array("name"=>"subpage", "value"=>"createArticle");
        $createArticleLink = $this->makeURL($linkParam);
        return $this->twig->render("myarticles.twig", array("articles"=>$this->getArticles(), "createArticleLink"=>$createArticleLink));
    }

    /**
     * Finds all user's articles in DB and formats them for twig template
     * @return array array of formatted article list
     */
    private function getArticles(){
        $username = $this->login->getLoggedUserInformation()["username"];
        $articlesQueryResult = $this->article->getUsersArticles($username);

        $articles = array();
        foreach ($articlesQueryResult as $articleResult){
            $article = array();
            $linkParam = array();
            $linkParam[] = array("name"=>"page", "value"=>"home");
            $linkParam[] = array("name"=>"subpage", "value"=>"viewArticle");
            $linkParam[] = array("name"=>"articleID", "value"=>$articleResult[ARTICLE_ID_COLUMN]);
            $article["link"] = $this->makeURL($linkParam);

            $linkParam = array();
            $linkParam[] = array("name"=>"page", "value"=>"loggedUser");
            $linkParam[] = array("name"=>"subpage", "value"=>"updateArticle");
            $linkParam[] = array("name"=>"articleID", "value"=>$articleResult[ARTICLE_ID_COLUMN]);
            $updateArticleLink = $this->makeURL($linkParam);
            $article["updateLink"] = $updateArticleLink;

            $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
            $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];
            if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 0){
                $article["status"] = "In review";
            }
            else if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 1) {
                $article["status"] = "Published";
            }
            else if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 2) {
                $article["status"] = "Refused";
            }

            $articleReviewsQueryResult = $this->review->getArticleReviews($articleResult[ARTICLE_ID_COLUMN]);
            $reviewCount = count($articleReviewsQueryResult);

            if($reviewCount > 0) {
                $article["numberOfReviews"] = $reviewCount;
            }
            else {
                $article["numberOfReviews"] = 1;
            }
            $article["reviews"] = $this->getReviews($articleReviewsQueryResult);
            $articles[] = $article;
        }
        return $articles;
    }

    /**
     * Formats the review query result for twig template
     * @param $reviewsResult result of review query
     * @return array formatted review
     */
    private function getReviews($reviewsResult){
        $reviews = array();
        foreach ($reviewsResult as $reviewResult) {
            $review = array();
            $review["expertise"] = $reviewResult[REVIEW_EXPERTISE_COLUMN];
            $review["language"] = $reviewResult[REVIEW_LANGUAGE_COLUMN];
            $review["originality"] = $reviewResult[REVIEW_ORIGINALITY_COLUMN];
            if($reviewResult[REVIEW_RESULT_COLUMN] != 0){
                $review["result"] = "Accept";
            }
            else {
                $review["result"] = "Refuse";
            }
            $reviews[] = $review;
        }
        return $reviews;
    }
}