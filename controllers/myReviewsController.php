<?php

/**
 * Class myReviewsController controller of a myReviews page
 */
class myReviewsController extends baseController
{
    /**
     * Calls a method for rendering the page
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        return $this->renderContent(null);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $userID = $this->user->getUserID($this->login->getLoggedUserInformation()["username"]);

        //TO REVIEW
        $toReviewQueryResult = $this->article->getArticlesToReview($userID);

        $toReview = array();
        foreach ($toReviewQueryResult as $toReviewResult){
            $article = array();
            $article["title"] = $toReviewResult[ARTICLE_TITLE_COLUMN];
            $article["date"] = $toReviewResult[ARTICLE_DATE_COLUMN];
            $articleID = $toReviewResult[ARTICLE_ID_COLUMN];
            $articleLink = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleID)));
            $article["articleLink"] = $articleLink;
            $author = $this->user->getUsername($toReviewResult[ARTICLE_USER_FK_COLUMN]);

            $reviewLink = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"createReview"), array("name"=>"articleID", "value"=>$articleID)));
            $article["author"] = $author;
            $article["reviewLink"] = $reviewLink;

            $index = count($toReview);
            $toReview[$index] = $article;
        }

        //FINISHED REVIEWS
        $finishedReviewsQueryResult = $this->article->getReviewedArticles($userID);

        $finishedReviews = array();
        foreach ($finishedReviewsQueryResult as $finishedReviewResult){
            $finishedReview = array();
            $finishedReview["title"] = $finishedReviewResult[ARTICLE_TITLE_COLUMN];
            $finishedReview["date"] = $finishedReviewResult[ARTICLE_DATE_COLUMN];
            $finishedReview["expertise"] = $finishedReviewResult[REVIEW_EXPERTISE_COLUMN];
            $finishedReview["language"] = $finishedReviewResult[REVIEW_LANGUAGE_COLUMN];
            $finishedReview["originality"] = $finishedReviewResult[REVIEW_RESULT_COLUMN];
            $articleID = $finishedReviewResult[ARTICLE_ID_COLUMN];
            $articleLink = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleID)));
            $finishedReview["articleLink"] = $articleLink;

            if($finishedReviewResult[REVIEW_RESULT_COLUMN] == 0) {
                $finishedReview["result"] = "Refuse";
            }
            else {
                $finishedReview["result"] = "Accept";
            }

            $reviewID = $finishedReviewResult[REVIEW_ID_COLUMN];
            $reviewLink = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"viewReview"), array("name"=>"reviewID", "value"=>$reviewID)));
            $finishedReview["viewLink"] = $reviewLink;
            $authorUsername = $this->user->getUsername($finishedReviewResult[ARTICLE_USER_FK_COLUMN]);
            $finishedReview["author"] = $authorUsername;

            $finishedReviews[] = $finishedReview;
        }
        return $this->twig->render("myreview.twig", array("toReview" => $toReview, "finishedReviews"=>$finishedReviews));
    }
}