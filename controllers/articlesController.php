<?php

/**
 * Class articlesController handles main page with all published articles
 */
class articlesController extends baseController
{

    const articleOnPage = 8;

    /**
     * Handles the pagination parameter and then call renderContent method for rendering the content
     * @return string rendered page
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {

        $paginationResult = $this->getPagination();
        if($paginationResult["paginationContent"] != null) {
            $pagination = $paginationResult["paginationContent"];
        }
        else {
            $pagination = "";
        }

        $content = array();
        $content["startArticle"] = $paginationResult["startArticle"];

        $render = $this->renderContent($content);
        $render .= $pagination;
        return $render;
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $startArticle = $content["startArticle"];
        if(isset($_REQUEST["categoryID"])){
            $categoryID = $_REQUEST["categoryID"];
            $articlesResult = $this->article->getArticlesByCategory($startArticle, self::articleOnPage, $categoryID);
        }
        else {
            $articlesResult = $this->article->getArticles($startArticle, self::articleOnPage);
        }

        $articles = array();
        foreach ($articlesResult as $articleResult){
            $article = array();
            $article["userIconPath"] = "img/user_icon.png";
            $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
            $article["author"] = $articleResult[USER_NAME_COLUMN];
            $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];
            $article["description"] = $articleResult[ARTICLE_OVERVIEW_COLUMN];
            $article["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleResult[ARTICLE_ID_COLUMN])));


            $categoriesQueryResult = $this->article->getArticleCategories($articleResult[ARTICLE_ID_COLUMN]);

            $categoriesArticle = array();
            foreach ($categoriesQueryResult as $categoryResult){
                $category = array();
                $category["name"] = $categoryResult[CATEGORY_NAME_COLUMN];
                $category["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"articles"),array("name"=>"categoryID", "value"=>$categoryResult[CATEGORY_ID_COLUMN])));
                $index = count($categoriesArticle);
                $categoriesArticle[$index] = $category;
            }

            $article["categories"] = $categoriesArticle;

            array_push($articles, $article);
        }

        return $this->twig->render("article_overview.twig", array("articles"=>$articles));
    }

    /**
     * This method do all calculation necessary for pagination and also prepare HTMl code for pagination
     * @return array array of pagination parameter and HTML code
     */
    private function getPagination(){
        if(isset($_REQUEST["pagination"])){
            $pagination = $_REQUEST["pagination"];
        }
        else {
            $pagination = 1;
        }

        if(isset($_REQUEST["categoryID"])){
            $categoryID = $_REQUEST["categoryID"];
            $articleCount = $this->article->getTotalNumberOfPublishedArticlesInCategory($categoryID);
        }
        else {
            $articleCount = $this->article->getTotalNumberOfPublishedArticles();
        }

        $totalPages = ceil($articleCount / self::articleOnPage);
        if($pagination < 1){
            $pagination = 1;
        }
        else if($pagination > $totalPages) {
            $pagination = $totalPages;
        }

        $startArticle = ($pagination-1)*self::articleOnPage;
        if($startArticle < 0) $startArticle = 0;

        if($totalPages > 1){
            $paginationContent = "<ul class='pagination justify-content-center'>";
            foreach (range(1, $totalPages) as $pageNr){
                if($pageNr == $pagination) $active = "active"; else $active = "";
                if ($pageNr == 1 || $pageNr == $totalPages || ($pageNr >= $pagination - 2 && $pageNr <= $pagination + 2)) {
                    if(isset($categoryID)){
                        $linkPar = array();
                        array_push($linkPar, array("name"=>"page", "value"=>"home"));
                        array_push($linkPar, array("name"=>"subpage", "value"=>"articles"));
                        array_push($linkPar, array("name"=>"categoryID", "value"=>$categoryID));
                        array_push($linkPar, array("name"=>"pagination", "value"=>$pageNr));
                        $link = $this->makeURL($linkPar);
                        $paginationContent .= "<li class='page-item ".$active."'><a class='page-link' href='$link'>$pageNr</a></li>";

                    }
                    else {
                        $linkPar = array();
                        array_push($linkPar, array("name"=>"page", "value"=>"home"));
                        array_push($linkPar, array("name"=>"subpage", "value"=>"articles"));
                        array_push($linkPar, array("name"=>"pagination", "value"=>$pageNr));
                        $link = $this->makeURL($linkPar);
                        $paginationContent .= "<li class='page-item ".$active."'><a class='page-link' href='$link'>".$pageNr."</a></li>";
                    }
                }
            }
        }
        else {
            $paginationContent = null;
        }

        return array("startArticle"=>$startArticle, "paginationContent"=>$paginationContent);
    }
}