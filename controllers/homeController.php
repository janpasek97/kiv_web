<?php

/**
 * Class homeController renders the menu and the page base and decides which subpage controller to call
 */
class homeController extends baseController
{
    /**
     * Evaluates the selected subpage and then it calls the subpage controller
     * Also calls rendering of the homepage template
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        $subpages = array("viewArticle"=>"View article", "articles"=>"Display articles", "displayUsers"=>"Display users", "error"=>"Error 404", "accessError");

        //get subpage
        if(isset($_REQUEST["subpage"])){
            $subpage = $_REQUEST["subpage"];
        }
        else {
            $subpage = "articles";
        }

        if(!array_key_exists($subpage, $subpages)){
            $subpage = "error";
        }

        $controller = $subpage."Controller";
        include_once ("controllers/$controller.php");

        $$controller = new $controller($this->twig, $this->models);
        $content = $$controller->indexAction();

        return $this->renderContent($content);
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $params = array();
        $params["categories"] = $this->getCategories();
        $params["login"] = $this->getLogin();
        $params["selectedCategory"] = $this->getSelectedCategory();
        $params["homeLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"home")));
        $params["authorsLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"displayUsers")));
        $params["content"] = $content;
        return $this->twig->render("homepage.twig", $params);
    }

    /**
     * Collects and formats all categories form twig template
     * @return array array of formatted categories
     */
    private function getCategories(){
        $categories = array();
        $categoriesResult = $this->category->getAllCategories();
        foreach ($categoriesResult as $categoryResult){
            $category = array();
            $categoryID = $categoryResult[CATEGORY_ID_COLUMN];
            $category["name"] = $categoryResult[CATEGORY_NAME_COLUMN];
            $category["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"articles"), array("name"=>"categoryID", "value"=>$categoryID)));
            array_push($categories, $category);
        }
        return $categories;
    }

    /**
     * Creates the login button or user section link according to if user is logged in
     * @return string content of login/user section button
     */
    private function getLogin(){
        if($this->login->isUserLogged()){
            $userSectionLink = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser")));
            $userLogoutLink = $this->makeURL(array(array("name"=>"page", "value"=>"login")));
            $login = "<form method='post' action='$userLogoutLink'><a style='color: #007bff' class='btn btn-link' href='$userSectionLink'>User section</a><button class='btn btn-link' name='action' value='logout'>Logout</button></form>";
        }
        else {
            $loginPage = array("name"=>"page", "value"=>"login");
            $userLoginLink = $this->makeURL(array($loginPage));
            $login = "<a href='$userLoginLink'>Login/Signup</a>";
        }
        return $login;
    }

    /**
     * Getter of currently selected category name
     * @return mixed|null selected categorys
     */
    private function getSelectedCategory(){
        if(isset($_REQUEST["categoryID"])){
            $categoryID = $_REQUEST["categoryID"];
           return $this->category->getCategoryNameByID($categoryID);
        }
        else {
            return null;
        }
    }

}