<?php
class updateArticleController extends baseController
{
    /**
     * Decides which action to perform and the call page renderer
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        $content = array();
        $content["success"] = "";

        if(isset($_POST["action"])){
            $action = $_POST["action"];
            if($action == "updateArticle"){
                if($this->updateArticle()){
                    $content["success"] = "success";
                }
                else {
                    $content["success"] = "failed";
                }
            }
        }

        if(isset($_REQUEST["articleID"])){
            $articleID = $_REQUEST["articleID"];
            $content["articleID"] = $articleID;
            return $this->renderContent($content);
        }
        else {
            //TODO error
        }
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        if($content["success"] == "success") {
            $success = "success";
        }
        else {
            $success = null;
        }

        if($content["success"] == "failed"){
            $failed = "failed";
        }
        else {
            $failed = null;
        }
        $linkParam = array();
        $linkParam[] = array("name"=>"page", "value"=>"loggedUser");
        $linkParam[] = array("name"=>"subpage", "value"=>"updateArticle");
        $linkParam[] = array("name"=>"articleID", "value"=>$content["articleID"]);
        $updateArticleLink = $this->makeURL($linkParam);

        $article["ID"] = $content["articleID"];

        $articleResult = $this->article->getArticleByID($article["ID"]);

        $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
        $article["description"] = $articleResult[ARTICLE_OVERVIEW_COLUMN];
        $article["content"] = $articleResult[ARTICLE_TEXT_COLUMN];

        $articleCategories = $this->article->getArticleCategories($article["ID"]);


        return $this->twig->render("update_article.twig", array("categories"=>$this->getCategories($articleCategories), "updateArticleLink"=>$updateArticleLink, "success"=>$success, "failed"=>$failed, "article"=>$article));
    }

    /**
     * Finds and formats all possible article categories
     * @return array all possible categories
     */
    private function getCategories($selected){
        $selected_categories = array();
        foreach ($selected as $selectedCategory){
            $selected_categories[$selectedCategory[CATEGORY_NAME_COLUMN]] = $selectedCategory[CATEGORY_ID_COLUMN];
        }

        $categoriesResult = $this->category->getAllCategories();
        $categories = array();
        foreach ($categoriesResult as $categoryResult){
            $category = array();
            $category["name"] = $categoryResult[CATEGORY_NAME_COLUMN];
            if(array_key_exists($category["name"], $selected_categories)){
                $category["selected"] = "selected";
            }
            else {
                $category["selected"] = "";
            }
            $categories[] = $category;
        }
        return $categories;
    }

    /**
     * Updates the article
     * @return bool true if update was successful
     */
    private function updateArticle(){
        $allSet = true;
        if(!isset($_POST["title"])){
            $allSet = false;
        }
        if(!isset($_POST["categories"])){
            $allSet = false;
        }
        if(!isset($_POST["articleDescription"])){
            $allSet = false;
        }
        if(!isset($_POST["articleText"])){
            $allSet = false;
        }
        if(!isset($_POST["articleID"])){
            $allSet = false;
        }
        if($allSet) {
            $title = $_REQUEST["title"];
            $categories = $_REQUEST["categories"];
            $description = $_REQUEST["articleDescription"];
            $text = $_REQUEST["articleText"];
            $articleID = $_POST["articleID"];

            //if there is not a request to update a file
            if(empty($_FILES["pdfFile"]["name"])){
                $this->category->unassignAllCategories($articleID);
                $this->category->assignCategoriesToArticle($categories, $articleID);
                $this->article->updateArticle($articleID, $title, $description, $text);
                return true;
            }
            //if there is a request to update the file
            else {
                $targetDir = "uploads/";
                $uploadedFile = basename($_FILES["pdfFile"]["name"]);
                $uploadOK = true;
                $fileType = strtolower(pathinfo($uploadedFile, PATHINFO_EXTENSION));

                //only PDF files are allowed to be uploaded
                if($fileType != "pdf"){
                    $uploadOK = false;
                }

                //uploaded file is ok
                if($uploadOK){
                    //update database entries
                    $this->category->unassignAllCategories($articleID);
                    $this->category->assignCategoriesToArticle($categories, $articleID);
                    $fileName = basename($_FILES["pdfFile"]["name"], ".pdf");
                    $fileName .= $articleID.".pdf";

                    //deletes an original file and finish upload of the new file
                    $originalFile = "uploads/".$this->article->getArticleByID($articleID)[ARTICLE_FILE];
                    unlink($originalFile);

                    $this->article->updateArticleWithPDF($articleID, $title, $description, $text, $fileName);
                    $targetFile = $targetDir.$fileName;
                    move_uploaded_file($_FILES["pdfFile"]["tmp_name"], $targetFile);
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        else {
            return false;
        }
    }

}