<?php

/**
 * Class createArticleController handles page for creating an article
 */
class createArticleController extends baseController
{
    /**
     * Decides which action to perform and the call page renderer
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if(isset($_POST["action"])){
            $action = $_POST["action"];
            if($action=="createArticle"){
                if($this->createArticle()){
                    $content = "success";
                }
                else {
                    $content = "failed";
                }
            }
        }
        else {
            $content = "";
        }
        return $this->renderContent($content);
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        if($content == "success") {
            $success = "success";
        }
        else {
            $success = null;
        }

        if($content == "failed"){
            $failed = "failed";
        }
        else {
            $failed = null;
        }
        $linkParam = array();
        $linkParam[] = array("name"=>"page", "value"=>"loggedUser");
        $linkParam[] = array("name"=>"subpage", "value"=>"createArticle");
        $createArticleLink = $this->makeURL($linkParam);
        return $this->twig->render("newarticle.twig", array("categories"=>$this->getCategories(), "createArticleLink"=>$createArticleLink, "success"=>$success, "failed"=>$failed));
    }

    /**
     * Finds and formats all possible article categories
     * @return array all possible categories
     */
    private function getCategories(){
        $categoriesResult = $this->category->getAllCategories();
        $categories = array();
        foreach ($categoriesResult as $categoryResult){
            $category = array();
            $category["name"] = $categoryResult[CATEGORY_NAME_COLUMN];
            $categories[] = $category;
        }
        return $categories;
    }

    /**
     * Creates new article in DB
     * Handles file upload
     * @return bool true if article was created successfully
     */
    private function createArticle(){
        $allSet = true;
        if(!isset($_POST["title"])){
            $allSet = false;
        }
        if(!isset($_POST["categories"])){
            $allSet = false;
        }
        if(!isset($_POST["articleDescription"])){
            $allSet = false;
        }
        if(!isset($_POST["articleText"])){
            $allSet = false;
        }
        if($allSet) {
            $title = $_REQUEST["title"];
            $categories = $_REQUEST["categories"];
            $description = $_REQUEST["articleDescription"];
            $text = $_REQUEST["articleText"];

            $targetDir = "uploads/";
            $uploadedFile = basename($_FILES["pdfFile"]["name"]);
            $uploadOK = true;
            $fileType = strtolower(pathinfo($uploadedFile, PATHINFO_EXTENSION));

            //only PDF files are allowed to be uploaded
            if($fileType != "pdf"){
                $uploadOK = false;
            }

            if($uploadOK) {

                $userID = $this->user->getUserID($this->login->getLoggedUserInformation()["username"]);
                $articleId = $this->article->insertArticle($title, $text, $description, $userID);
                $this->category->assignCategoriesToArticle($categories, $articleId);

                $fileName = basename($_FILES["pdfFile"]["name"], ".pdf");
                $fileName .= $articleId.".pdf";
                $targetFile = $targetDir.$fileName;
                move_uploaded_file($_FILES["pdfFile"]["tmp_name"], $targetFile);
                $this->article->addFileToArticle($articleId, $fileName);
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}