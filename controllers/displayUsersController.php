<?php

/**
 * Class displayUsersController handles page with author list
 */
class displayUsersController extends baseController
{

    /**
     * Only returns rendered content
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        return $this->renderContent(null);
    }

    /**
     * Finds and formats the list of all authors
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $allAuthorsQueryResult = $this->user->getAllUsersWithRight("author");
        $authors = array();
        foreach ($allAuthorsQueryResult as $authorResult){
            $author = array();
            $author["iconPath"] = "img/user_icon.png";
            $author["name"] = $authorResult[USER_NAME_COLUMN];

            $authorID = $authorResult[USER_ID_COLUMN];


            $author["numberOfArticles"] = $this->article->getNumberOfUserPublishedArticles($authorID)["total"];

            $authorCategoriesResult = $this->user->getUserCategories($authorID);

            //formatting categories into an array for twig
            $userCategories = array();
            foreach ($authorCategoriesResult as $categoryResult) {
                $index = count($userCategories);
                $userCategories[$index] = $categoryResult[CATEGORY_NAME_COLUMN];
            }

            $author["categories"] = $userCategories;

            $index = count($authors);
            $authors[$index] = $author;
        }

        return $this->twig->render("user_card.twig", array("users"=>$authors));
    }

}