<?php

/**
 * Class viewReviewController renders the viewReview page
 */
class viewReviewController extends baseController
{
    /**
     * Renders the page content
     * @return string rendered page content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if(isset($_REQUEST["reviewID"])){
            $reviewID = $_REQUEST["reviewID"];
            return $this->renderContent($reviewID);
        }
    else {
        header("Location: index.php");
        die();
        }
    }

    /**
     * Renders the page content
     * @param $content content rendering parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public  function renderContent($content)
    {
        //REVIEW
        $review = $this->getReviewInformation($content);
        //ARTICLE
        $article = $this->getArticleInformation($content);

        return $this->twig->render("review.twig", array("review"=>$review, "article"=>$article));
    }

    /**
     * Finds and formats the article information
     * @param $reviewID ID of review
     * @return array formatted article information
     */
    private function getArticleInformation($reviewID){
        $articleResult = $this->review->getArticlesReviewedByReview($reviewID);

        $articleID = $articleResult[ARTICLE_ID_COLUMN];
        $article = array();
        $articleLink = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleID)));
        $article["link"] = $articleLink;
        $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
        $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];

        $articleAuthor = array();
        $articleAuthor["picture"] = "img/user_icon.png";
        $articleAuthor["name"] = $articleResult[USER_NAME_COLUMN];

        $article["author"] = $articleAuthor;

        $articleCategoriesResult = $this->article->getArticleCategories($articleID);

        $categories = array();
        foreach ($articleCategoriesResult as $categoryResult){
            $index = count($categories);
            $categories[$index] = $categoryResult[CATEGORY_NAME_COLUMN];
        }

        $article["categories"] = $categories;
        return $article;
    }

    /**
     * Finds and formats the review information
     * @param $reviewID ID of review
     * @return array formatted review information
     */
    private function getReviewInformation($reviewID){
        $reviewResult = $this->review->getReview($reviewID);

        $review = array();
        $review["date"] = $reviewResult[REVIEW_DATE_COLUMN];
        $review["expertise"] = $reviewResult[REVIEW_EXPERTISE_COLUMN];
        $review["language"] = $reviewResult[REVIEW_LANGUAGE_COLUMN];
        $review["originality"] = $reviewResult[REVIEW_ORIGINALITY_COLUMN];
        if($reviewResult[REVIEW_RESULT_COLUMN] == 0) {
            $review["result"] = "Refuse";
        }
        else {
            $review["result"] = "Accept";
        }
        $review["comment"] = $reviewResult[REVIEW_COMMENT_COLUMN];

        $reviewAuthorUsername =  $this->review->getReviewAuthor($reviewID);
        $reviewAuthor = array();
        $reviewAuthor["name"] = $reviewAuthorUsername;
        $reviewAuthor["picture"] = "img/user_icon.png";
        $review["author"] = $reviewAuthor;

        return $review;
    }
}