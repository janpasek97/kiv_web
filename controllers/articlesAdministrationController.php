<?php

/**
 * Class articlesAdministrationController handles rendering of article administration page and also all it's actions
 */
class articlesAdministrationController extends baseController
{


    private $warning = "";

    /**
     * Decides which action should be handled and then it calls a method for rendering the content
     * @return string rendered content of the page
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if(isset($_POST["action"])){
            $action = $_POST["action"];
            if($action == "delete"){
                $this->deleteArticle();
            }
            else if($action == "publish"){
                $this->publishArticle();
            }
            else if($action == "refuse"){
                $this->refuseArticle();
            }
            else if($action == "assign"){
                $this->assignReview();
            }
            else if($action == "unassign"){
                $this->unassignReview();
            }
        }
        return $this->renderContent(null);
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $reviewers = $this->getAllReviewers();
        $articlesInReview = $this->getArticlesInReview();
        $publishedArticles = $this->getReviewedArticles();

        return $this->twig->render("articles_administration.twig", array("reviewers" => $reviewers, "articlesInReview" => $articlesInReview, "publishedArticles" => $publishedArticles, "warning"=>$this->warning));
    }

    /**
     * Finds and formats all possible reviewers
     * @return array all reviewers
     */
    public function getAllReviewers(){
        //finding all possible reviewer and inserting them into reviewer list
        $reviewersQueryResult = $this->user->getAllUsersWithRight("reviewer");
        $reviewers = array();
        foreach ($reviewersQueryResult as $reviewerResult) {
            $reviewerIndex = count($reviewers);
            $reviewers[$reviewerIndex] = $reviewerResult[USER_NAME_COLUMN];
        }
        return $reviewers;
    }

    /**
     * Finds all articles that are in review and formats them for the twig template
     * @return array formatted array of articles in review
     */
    public function getArticlesInReview(){
        $articlesInReviewQueryResult = $this->article->getArticlesInReview();

        //For each article we will find all information necessary for rendering twig template
        $articlesInReview = array();
        foreach ($articlesInReviewQueryResult as $articleResult) {
            $article = array();
            $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
            $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];

            //Getting username of an author
            $authorName = $this->user->getUsername($articleResult[ARTICLE_USER_FK_COLUMN]);
            $article["author"] = $authorName;

            //Collecting all finished reviews of current article
            $articleReviewsQueryResult = $this->review->getArticleReviews($articleResult[ARTICLE_ID_COLUMN]);
            $articleReviewsCount = count($articleReviewsQueryResult);
            $articleReviewsCount += 1;

            //Collecting all reviews that are already assigned
            $assignReviewsQueryResult = $this->review->getUnfinishedArticleReviews($articleResult[ARTICLE_ID_COLUMN]);
            $numberOfAssignedReviews = count($assignReviewsQueryResult);

            //Finding usernames of reviewers that are assigned to review the article
            $assignedReviews = array();
            foreach ($assignReviewsQueryResult as $assignedReviewResult){
                $assignedReview = array();
                $assignedReview["username"] = $assignedReviewResult[USER_NAME_COLUMN];
                $assignedReview["userID"] = $this->user->getUserID($assignedReviewResult[USER_NAME_COLUMN]);

                $index = count($assignedReviews);
                $assignedReviews[$index] = $assignedReview;
            }

            $article["ID"] = $articleResult[ARTICLE_ID_COLUMN];
            $article["numberOfReviews"] = $articleReviewsCount+$numberOfAssignedReviews;
            $article["actionLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"articlesAdministration")));
            $article["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleResult[ARTICLE_ID_COLUMN])));

            if ($articleReviewsCount < 4) {
                $article["publishDisabled"] = "disabled";
            } else {
                $article["publishDisabled"] = "";
            }

            //Collecting results of finished reviews
            $totalSum = 0;
            $reviews = array();
            foreach ($articleReviewsQueryResult as $reviewResult) {
                $review = array();
                $review["expertise"] = $reviewResult[REVIEW_EXPERTISE_COLUMN];
                $review["originality"] = $reviewResult[REVIEW_ORIGINALITY_COLUMN];
                $review["language"] = $reviewResult[REVIEW_LANGUAGE_COLUMN];
                $review["sum"] = $review["expertise"]+$review["originality"]+$review["language"];
                $review["author"] = $this->review->getReviewAuthor($reviewResult[REVIEW_ID_COLUMN]);
                if ($reviewResult[REVIEW_RESULT_COLUMN] == 0) {
                    $review["result"] = "Refuse";
                } else {
                    $review["result"] = "Accept";
                }
                $review["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"viewReview"), array("name"=>"reviewID", "value"=>$reviewResult[REVIEW_ID_COLUMN])));
                $reviews[] = $review;
                $totalSum += $review["sum"];
            }

            if(count($articleReviewsQueryResult) != 0) {
                $totalAverage = $totalSum / count($articleReviewsQueryResult);
                $article["reviewAverage"] = number_format($totalAverage, 2);
            }
            else {
                $article["reviewAverage"] = "N/A";
            }


            $article["assignedReviews"] = $assignedReviews;
            $article["reviews"] = $reviews;
            $articlesInReview[] = $article;
        }
        return $articlesInReview;
    }

    /**
     * Finds in DB and formats all reviewed articles
     * @return array reviewed articles formatted for twig template
     */
    public function getReviewedArticles(){
        //Finding all articles that already went through decision if they will be published or refused
        $publishedArticlesQueryResult = $this->article->getAllArticlesWithDecision();

        //For each found article we need to collect data to be displayed on page
        $counter = 0;
        $publishedArticles = array();
        foreach ($publishedArticlesQueryResult as $articleResult) {
            $article = array();
            $article["ID"] = $articleResult[ARTICLE_ID_COLUMN];
            $article["title"] = $articleResult[ARTICLE_TITLE_COLUMN];
            $article["date"] = $articleResult[ARTICLE_DATE_COLUMN];
            $article["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$articleResult[ARTICLE_ID_COLUMN])));
            $article["actionLink"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"articlesAdministration")));

            if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 0){
                $article["status"] = "In review";
            }
            else if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 1) {
                $article["status"] = "Published";
            }
            else if($articleResult[ARTICLE_PUBLISHED_COLUMN] == 2) {
                $article["status"] = "Refused";
            }

            //Searching for author username
            $authorName = $this->user->getUsername($articleResult[ARTICLE_USER_FK_COLUMN]);
            $article["author"] = $authorName;

            //Searching for all reviews of an article
            $articleReviewsQueryResult = $this->review->getArticleReviews($articleResult[ARTICLE_ID_COLUMN]);
            $articleReviewsCount = count($articleReviewsQueryResult);

            $article["numberOfReviews"] = $articleReviewsCount;


            //Collecting and formatting review data
            $temp_cnt = -1;
            $totalSum = 0;
            foreach ($articleReviewsQueryResult as $reviewResult) {
                $review = array();
                $review["expertise"] = $reviewResult[REVIEW_EXPERTISE_COLUMN];
                $review["originality"] = $reviewResult[REVIEW_ORIGINALITY_COLUMN];
                $review["language"] = $reviewResult[REVIEW_LANGUAGE_COLUMN];
                $review["sum"] = $review["expertise"]+$review["originality"]+$review["language"];
                $totalSum += $review["sum"];
                $review["author"] = $this->review->getReviewAuthor($reviewResult[REVIEW_ID_COLUMN]);

                if ($reviewResult[REVIEW_RESULT_COLUMN] == 0) {
                    $review["result"] = "Refuse";
                } else {
                    $review["result"] = "Accept";
                }
                $review["link"] = $this->makeURL(array(array("name"=>"page", "value"=>"loggedUser"), array("name"=>"subpage", "value"=>"viewReview"), array("name"=>"reviewID", "value"=>$reviewResult[REVIEW_ID_COLUMN])));
                if ($temp_cnt < 0) {
                    $firstReview = $review;
                } else {
                    $otherReviews[$temp_cnt] = $review;
                }
                $temp_cnt++;
            }
            $article["otherReviews"] = $otherReviews;
            $article["firstReview"] = $firstReview;

            $totalAverage = $totalSum/count($articleReviewsQueryResult);
            $article["reviewAverage"] = number_format($totalAverage, 2);

            $publishedArticles[$counter] = $article;
            $counter++;
        }
        return $publishedArticles;
    }

    /**
     * Deletes an article
     */
    public function deleteArticle(){
        if(isset($_POST["articleID"])){
            $articleID = $_POST["articleID"];

            $article = $this->article->getArticleByID($articleID);
            if($article[ARTICLE_FILE != null]) {
                $articleFile = "uploads/" . $article[ARTICLE_FILE];
                unlink($articleFile);
            }


            $articleReviews = $this->review->getArticleReviews($articleID);
            $this->article->deleteArticle($articleID, $articleReviews);
        }
        else {
            $this->warning = "<div class='alert alert-danger' role='alert'>Article was not deleted successfully.</div>";
        }
    }

    /**
     * Handles assign review action
     */
    public function assignReview(){
        if(isset($_POST["username"]) && isset($_POST["articleID"])){
            $username = $_POST["username"];
            $articleID = $_POST["articleID"];
            $userID = $this->user->getUserID($username);

            $authorID = $this->article->getArticleByID($articleID)[ARTICLE_USER_FK_COLUMN];

            if($userID != $authorID){
                if(!$this->review->assignReview($userID, $articleID)){
                    $this->warning = "<div class='alert alert-danger' role='alert'>Review of this article was already assigned to this author</div>";
                }
            }
            else {
                $this->warning = "<div class='alert alert-danger' role='alert'>You can not assign review to of article to it's author</div>";
            }
        }
        else {
            $this->warning = "<div class='alert alert-danger' role='alert'>Review was not assigned successfully</div>";
        }
    }

    /**
     * Handles refuse article action
     */
    public function refuseArticle(){
        if(isset($_POST["articleID"])) {
            $articleID = $_POST["articleID"];
            $this->article->updateArticlePublishState(2, $articleID);
        }
        else {
            $this->warning = "<div class='alert alert-danger' role='alert'>Article was not refused successfully.</div>";
        }
    }

    /**
     * Handles publish article action
     */
    public function publishArticle(){
        if(isset($_POST["articleID"])) {
            $articleID = $_POST["articleID"];
            $this->article->updateArticlePublishState(1, $articleID);
        }
        else {
            $this->warning = "<div class='alert alert-danger' role='alert'>Article was not published successfully.</div>";
        }
    }

    /**
     * Unassigns a review from user
     */
    public function unassignReview(){
        if(isset($_POST["userID"]) && isset($_POST["articleID"])){
            $userID = $_POST["userID"];
            $articleID = $_POST["articleID"];
            $this->review->deleteAssignedReview($userID, $articleID);
        }
        else {
            $this->warning = "<div class='alert alert-danger' role='alert'>Review was not unassigned successfully.</div>";
        }

    }
}