<?php

/**
 * Class createReviewController handles page for creating review
 */
class createReviewController extends baseController
{

    private $warning = "";

    /**
     * First it handles the selected action and the it calls page renderer
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function indexAction()
    {
        if (isset($_POST["action"])){
            $action = $_POST["action"];
            if($action == "createReview"){
                if($this->createReview()){
                    $this->warning = "<script>window.location.href = 'user/myReviews'</script>";
                }
                else {
                    $this->warning = "<div class='alert alert-danger' role='alert'>Review was not created successfully.</div>";
                }
            }
        }


        if(isset($_REQUEST["articleID"])){
            $articleID = $_REQUEST["articleID"];
            return $this->renderContent($articleID);
        }
        else {
            //TODO error
        }
    }

    /**
     * Renders twig template of the page
     * @param $content content parameters
     * @return string rendered content
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function renderContent($content)
    {
        $article = array();
        $articleLink = $this->makeURL(array(array("name"=>"page", "value"=>"home"), array("name"=>"subpage", "value"=>"viewArticle"), array("name"=>"articleID", "value"=>$content)));
        $article["link"] = $articleLink;

        $articleQueryResult = $this->article->getArticleByID($content);

        $article["title"] = $articleQueryResult[ARTICLE_TITLE_COLUMN];
        $article["ID"] = $content;

        $author = $articleQueryResult[USER_NAME_COLUMN];
        $article["author"] = $author;

        return $this->twig->render("create_review.twig", array("article" => $article, "warning"=>$this->warning));
    }

    /**
     * Creates new article in DB
     * @return bool
     */
    private function createReview(){
        $allset = true;
        if(!isset($_REQUEST["articleID"])){
            $allset = false;
        }
        if(!isset($_REQUEST["expertise"])){
            $allset = false;
        }
        if(!isset($_REQUEST["originality"])){
            $allset = false;
        }
        if(!isset($_REQUEST["language"])){
            $allset = false;
        }
        if(!isset($_REQUEST["result"])){
            $allset = false;
        }
        if(!isset($_REQUEST["comment"])){
            $allset = false;
        }
        if($allset) {
            $articleID = $_REQUEST["articleID"];
            $expertise = $_REQUEST["expertise"];
            $originality = $_REQUEST["originality"];
            $language = $_REQUEST["language"];
            $result = $_REQUEST["result"];
            $comment = $_REQUEST["comment"];

            $userID = $this->user->getUserID($this->login->getLoggedUserInformation()["username"]);
            $reviewID = $this->review->insertReview($result, $expertise, $language, $originality, $comment);
            $this->review->updateArticleReview($reviewID, $userID, $articleID);
            return true;
        }
        else {
            return false;
        }
    }
}