<?php

    //load TWIG and create its instance
    require_once "vendor/autoload.php";
    $loader = new Twig_Loader_Filesystem("templates");
    $twig = new Twig_Environment($loader, array());

    //import database settings and other helper objects
    include_once ("inc/db_settings.inc.php");
    include_once ("inc/url_settings.inc.php");
    include_once ("inc/Session.class.php");

    //include base model
    include_once ("models/baseModel.php");

    $models = array();

    //include login model handler
    include_once ("models/userLogin.php");
    $login = new UserLogin();
    $login->Connect();
    $models["login"] = $login;

    //include and create all other models and put them into an array
    include_once ("models/categoryModel.php");
    $categoryModel = new categoryModel();
    $categoryModel->Connect();
    $models["category"] = $categoryModel;

    include_once ("models/articleModel.php");
    $articleModel = new articleModel();
    $articleModel->Connect();
    $models["article"] = $articleModel;

    include_once ("models/userModel.php");
    $userModel = new userModel();
    $userModel->Connect();
    $models["user"] = $userModel;

    include_once ("models/reviewModel.php");
    $reviewModel = new reviewModel();
    $reviewModel->Connect();
    $models["review"] = $reviewModel;

    include_once ("models/userRightModel.php");
    $rightsModel = new userRightModel();
    $rightsModel->Connect();
    $models["right"] = $rightsModel;

    //get the page
    if(isset($_REQUEST["page"])){
        $page = $_REQUEST["page"];
    }
    else {
        $page = "home";
    }

    //available pages
    $pages = array("home"=>"Home page", "loggedUser"=>"Logged user", "login"=>"Login" , "error"=>"Error 404", "accessError"=>"Access restricted error");

    //check if a valid page is requested
    if(!array_key_exists($page, $pages)){
        $page="error";
    }
    else if((!$login->isUserLogged()) && ($page == "loggedUser")){
        $page="accessError";
    }

    //if a valid page is requested, pages's controller is created and its index method is called -> controller renders the content of the page
    if(array_key_exists($page, $pages)){
        include_once ("controllers/baseController.php");

        $controller = $page."Controller";
        include ("controllers/$controller.php");

        $$controller = new $controller($twig, $models);
        echo $$controller->indexAction();
    }

