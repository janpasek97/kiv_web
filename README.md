<H1>Informace o samostatné práci</h1>


<h2>1 Technologie</h2>
Povinně HTML5, CSS, PHP, MySQL (nebo jiná databáze) + volitelně šablony, JavaScript, AJAX apod.

<h2>2 Zadání samostatné práce</h2>
Možno volit mezi standardním a alternativními zadáními.

<b>2.1 Standardní zadání - webové stránky konferenčního systému</b>
Vaším úkolem bude vytvořit webové stránky konference.  Téma konference si můžete zvolit libovolné.
Uživateli systému jsou autoři příspěvků (vkládají abstrakty článků a PDF dokumenty), recenzenti příspěvků (hodnotí příspěvky) a administrátoři (spravují uživatele, přiřazují příspěvky recenzentům k hodnocení a rozhodují o publikování příspěvků). Každý uživatel se do systému přihlašuje prostřednictvím vlastního uživatelského jména a hesla. Nepřihlášený uživatel vidí pouze publikované příspěvky.
Nový uživatel se může do systému zaregistrovat, čímž získá status autora.
Přihlášený autor vidí svoje příspěvky a stav, ve kterém se nacházejí (v recenzním řízení / přijat +hodnocení / odmítnut +hodnocení). Své příspěvky může přidávat, editovat a volitelně i mazat. Rozhodnutí, zda autor může editovat či mazat publikované příspěvky je ponecháno na tvůrci systému.
Přihlášený recenzent vidí příspěvky, které mu byly přiděleny k recenzi, a může je hodnotit (nutně alespoň 3 kritéria hodnocení). Pokud příspěvek nebyl dosud publikován, tak své hodnocení může změnit.
Přihlášený administrátor spravuje uživatele (určuje jejich role a může uživatele zablokovat či smazat), přiřazuje neschválené příspěvky recenzentům k hodnocení (každý příspěvek bude recenzován minimálně třemi recenzenty) a na základě recenzí rozhoduje o publikování nebo odmítnutí příspěvku. Publikované příspěvky jsou automaticky zobrazovány ve veřejné části webu.
Databáze musí obsahovat alespoň 3 tabulky, které budou dostatečně naplněny daty tak, aby bylo možné předvést funkčnost aplikace.
<b>2.2 Alternativní zadání semestrální práce</b>
Alternativní zadání musí být předem schváleno cvičícím do stanoveného termínu. K tomuto účelu je nutné cvičícímu dodat písemný popis vytvářené aplikace, včetně ERA modelu databáze.
<b>2.2.1 Praktická realizace pro firmu</b>

Detailní popisy zadání jsou z důvodu průhlednosti zveřejněny na spoluprace.zcu.cz, kde si student může zadání zvolit.
Firma musí být informována, že daná realizace bude vytvořena v rámci studentské práce. Před konečným odevzdáním práce cvičícímu se k výslednému řešení musí firma písemně vyjádřit a toto vyjádření zaslat emailem cvičícímu. V rámci hodnocení SP může být k tomuto vyjádření přihlédnuto.
Firma si je v tomto případě vědoma, že výsledné řešení je veřejné a může být na základě žádosti poskytnuto a zveřejněno (což je aktuálně dáno zákonem o vysokých školách).
Případné utajení práce je možné pouze na základě souhlasu a doporučení vedoucího katedry a za podmínek stanovených katedrou. 
 

<h2>3 Nutné požadavky na všechny samostatné práce</h2>
Práce musí být osobně předvedena cvičícímu a po schválení odevzdána na CourseWare či Portál.
K práci musí být dodána dokumentace (viz dále) a skripty pro instalaci databáze (např. získané exportem databáze).
Aplikace musí dodržovat MVC architekturu.
Pro práci s databází musí být využito PDO nebo jeho ekvivalent a používány předpřipravené dotazy (prepared statements).
Web musí obsahovat responzivní design.
Web musí obsahovat ošetření proti základním typům útoku (XSS, SQL injection).
Web musí fungovat i s "ošklivými" URL adresami.
Aplikaci není možné realizovat s využitím PHP frameworků (zakázáno např. Nette, Symfony atd.).
Front-end je vhodné realizovat s využitím frameworku Bootstrap (getbootstrap.com), popř. lze využít jeho ekvivalent.
<b>3.1 Dokumentace</b>
K práci vytvořte dokumentaci, která bude obsahovat:

Vaše jméno, URL vytvořených stránek (pokud jsou zveřejněny na serveru students.kiv.zcu.cz či jinde), Váš email, datum vytvoření, název práce.
Popis použitých technologií - uveďte hlavně, ve které části jste kterou technologii použili.
Popis adresářové struktury aplikace - co je ve kterých adresářích a souborech.
Popis architektury aplikace - co mají na starosti které třídy (popř. lze využít i UML diagramy).
U alternativního zadání uveďte celé, cvičícím schválené zadání práce.
Dokumentaci netiskněte, ale odevzdejte ji ve formátu PDF.

<b>3.2 Kontrolní seznam před odevzdáním SP</b>
Zákaz frameworků - není použit Php framework jako celek (zákaz Nette, Symfony, Laravel atd.). Použití komponent je podmíněno schválením cvičícího. 
Github, Bitbucket  - práce je uložena na Githubu nebo jiném repozitáři (snadné body) a ideálně je vidět, že byl repozitář při vývoji používán.
Bootstrap - je použit Bootstrap či jeho ekvivalent. Obvykle bývá automaticky zajištěna responzivita (dohromady hodně bodů).
Zákaz plagiátorství - práci musí student vytvořit sám. Nelze ji tedy zkopírovat např. z Githubu a ani nelze "udat" starou práci ze střední školy (obvykle nesplňuje nutné požadavky). Plagiátorství je odhalováno automatizovaným systémem a hodnoceno odebráním zápočtu. 
 

<h2>4 Hodnocení samostatné práce a získání zápočtu</h2>
Hodnocení práce je rozděleno na povinné požadavky a volitelná rozšíření.
Odevzdání práce v průběhu cvičení v ZS je ohodnoceno bonusovými body.
Pro získání zápočtu je nutné splnit povinné požadavky a získat ze semestrální práce minimálně 20 bodů.
Detailní podmínky sdělí a vysvětlí každý cvičící na prvním cvičení.
5 Předvedení a odevzdání
Vytvořené webové stránky při předvádění cvičícímu poběží na počítači, ke kterému bude mít cvičící fyzický přístup (např. počítač v učebně nebo notebook, který máte s sebou). Webovým serverem bude buď "localhost" (127.0.0.1), nebo alternativně lze využít školní server students.kiv.zcu.cz a stránky nechat běžet tam.

Zdrojové kódy včetně dokumentace ve formátu PDF odevzdejte prostřednictvím odevzdávacího portletu na Portal či Courseware. Odevzdávejte vždy jeden archiv pojmenovaný PRIJMENI-JMENO.ZIP a v nejvyšší úrovni adresářové struktury mějte soubor readme.txt s popisem toho, co se ve kterém adresáři nalézá. Práci ale odevzdejte až po jejím předvedení a uznání.

<h2>Odevzdaná semestrální práce musí mimo jiné obsahovat (rekapitulace):</h2>

1. Dokumentaci v PDF.
2. Postup instalace webu, včetně skriptů pro vytvoření databáze a její naplnění testovacími daty.
3. Všechny přístupy na využívané externí služby.
4. Všechny soubory potřebné k provozu dané webové aplikace.
( 5. Soubor s vlastním, cvičícím schváleným zadáním práce ).
Nedodržení formálních požadavků na odevzdání má automaticky za následek nehodnocení samostatné práce.

Nutnou součástí odevzdání je osobní předvedení práce cvičícímu, které je možné pouze na cvičeních nebo ve stanovených termínech (cca 3 za zkouškové období).

<a href="https://courseware.zcu.cz/portal/studium/courseware/kiv/web/samostatna-prace/ukazka_samostatne_prace.html">Ukázka samostatné práce</a> <br>
<a href="https://courseware.zcu.cz/portal/studium/courseware/kiv/db1/samostatna-prace.html">Požadavky na samostatnou práci KIV/DB</a>