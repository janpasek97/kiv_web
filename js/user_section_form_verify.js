/**
 * Verifies the form for creating new article
 * @returns {boolean} true if all fields are filled in
 */
function verifyNewArticle() {
    var title = document.forms["articleForm"]["title"].value;
    var categories = $("#articleCategory option:selected");
    var description = document.forms["articleForm"]["articleDescription"].value;
    var text = document.forms["articleForm"]["articleText"].value;
    var inputFileLength = $("#pdf-file").get(0).files.length;

    //First reset the border colors
    $("#articleTitle").css("border-color","#ced4da");
    $("#articleCategory").css("border-color","#ced4da");
    $("#articleDescription").css("border-color","#ced4da");
    $("#articleContent").css("border-color","#ced4da");

    var formOK = true;

    //Validate the forms
    if(title == ""){
        $("#articleTitle").css("border-color","red");
        formOK = false;
    }
    if(categories.length == 0){
        $("#articleCategory").css("border-color","red");
        formOK = false;
    }
    if(description == ""){
        $("#articleDescription").css("border-color","red");
        formOK = false;
    }
    if(text == ""){
        $("#articleContent").css("border-color","red");
        formOK = false;
    }
    if(inputFileLength == 0){
        alert("Please select a pdf file with article content");
        formOK = false;
    }

    return formOK;
}

/**
 * Verifies the form for creating new review
 * @returns true {boolean} if all fields are filled in
 */
function verifyNewReview() {
    var expertise = document.forms["reviewForm"]["expertise"].value;
    var language = document.forms["reviewForm"]["language"].value;
    var originality = document.forms["reviewForm"]["originality"].value;
    var result = document.forms["reviewForm"]["result"].value;
    var comment = document.forms["reviewForm"]["comment"].value;

    var formOK = true;

    //First reset the border colors
    $("#expertise-form").css("border-color","#ced4da");
    $("#language-form").css("border-color","#ced4da");
    $("#originality-form").css("border-color","#ced4da");
    $("#result-form").css("border-color","#ced4da");
    $("#comment-form").css("border-color","#ced4da");


    //Validate the form
    if(expertise == ""){
        $("#expertise-form").css("border-color","red");
        formOK = false;
    }
    if(language == ""){
        $("#language-form").css("border-color","red");
        formOK = false;
    }
    if(originality == ""){
        $("#originality-form").css("border-color","red");
        formOK = false;
    }
    if(result == ""){
        $("#result-form").css("border-color","red");
        formOK = false;
    }
    if(comment == ""){
        $("#comment-form").css("border-color","red");
        formOK = false;
    }
    return formOK;
}

/**
 * Verifies the form for updating article
 * @returns {boolean} true if all fields are filled in
 */
function verifyUpdateArticle() {
    var title = document.forms["articleForm"]["title"].value;
    var categories = $("#articleCategory option:selected");
    var description = document.forms["articleForm"]["articleDescription"].value;
    var text = document.forms["articleForm"]["articleText"].value;

    //First reset the border colors
    $("#articleTitle").css("border-color","#ced4da");
    $("#articleCategory").css("border-color","#ced4da");
    $("#articleDescription").css("border-color","#ced4da");
    $("#articleContent").css("border-color","#ced4da");

    var formOK = true;

    //Validate the forms
    if(title == ""){
        $("#articleTitle").css("border-color","red");
        formOK = false;
    }
    if(categories.length == 0){
        $("#articleCategory").css("border-color","red");
        formOK = false;
    }
    if(description == ""){
        $("#articleDescription").css("border-color","red");
        formOK = false;
    }
    if(text == ""){
        $("#articleContent").css("border-color","red");
        formOK = false;
    }

    return formOK;
}
