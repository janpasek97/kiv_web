/**
 * Verifies the login form if both input fields are filled in
 * @returns {boolean} true if the form is valid
 */
function checkLoginForm() {
    var username = document.forms["loginForm"]["loginUsername"].value;
    var password = document.forms["loginForm"]["loginPassword"].value;

    var formOK = true;


    $("#login-username").css("border-color","#ced4da");
    $("#login-password").css("border-color","#ced4da");

    //Validate the form
    if(username == ""){
        $("#login-username").css("border-color","red");
        formOK = false;
    }
    if(password == ""){
        $("#login-password").css("border-color","red");
        formOK = false;
    }
    return formOK;
}

/**
 * Verifies the signup form if all required fields are filled in
 * @returns {boolean} true if all required fields are filled in and passwords matches.
 */
function checkSignupForm() {
    var username = document.forms["signupForm"]["signupUsername"].value;
    var password = document.forms["signupForm"]["signupPassword"].value;
    var rePassword = document.forms["signupForm"]["signupRePassword"].value;
    var email = document.forms["signupForm"]["signupEmail"].value;

    //First reset the border colors
    $("#signup-username").css("border-color","#ced4da");
    $("#signup-email").css("border-color","#ced4da");
    $("#signup-password").css("border-color","#ced4da");
    $("#signup-repassword").css("border-color","#ced4da");

    var formOK = true;

    //Validate the form
    if(username == ""){
        $("#signup-username").css("border-color","red");
        formOK = false;
    }
    if(email == ""){
        $("#signup-email").css("border-color","red");
        formOK = false;
    }
    if(password == ""){
        $("#signup-password").css("border-color","red");
        formOK = false;
    }
    if(rePassword == ""){
        $("#signup-repassword").css("border-color","red");
        formOK = false;
    }
    if(password.length < 5){
        $("#signup-password").css("border-color","red");
        formOK = false;
    }
    if(password != rePassword){
        $("#signup-password").css("border-color","red");
        $("#signup-repassword").css("border-color","red");
        formOK = false;
    }
    return formOK;
}