<?php

/**
 * Class reviewModel is used for handling request from reviews table
 */
class reviewModel extends baseModel
{
    /**
     * Getter of all reviews of given article
     * @param $articleID article ID of article for which you want to find reviews
     * @return array reviews of an article
     */
    public function getArticleReviews($articleID){
        $articleReviewsQuery = $this->connection->prepare("SELECT ".REVIEW_EXPERTISE_COLUMN.", ".REVIEW_LANGUAGE_COLUMN.", ".REVIEW_ORIGINALITY_COLUMN.", ".REVIEW_RESULT_COLUMN.", ".REVIEW_ID_COLUMN." FROM ".REVIEW_TABLE." 
                                                                    JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." = ".REVIEW_ID_COLUMN."
                                                                    JOIN ".ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." 
                                                                    WHERE ".ARTICLE_ID_COLUMN." = :articleID");
        $articleReviewsQuery->bindValue(':articleID', $articleID);
        $articleReviewsQuery->execute();
        return $articleReviewsQuery->fetchAll();
    }

    /**
     * Gets review by ID
     * @param $reviewID ID of review
     * @return mixed review
     */
    public function getReview($reviewID) {
        $where = array();
        $where["column"] = REVIEW_ID_COLUMN;
        $where["symbol"] = "=";
        $where["value"] = $reviewID;
        return $this->DBSelectOne(REVIEW_TABLE, "*", array($where));
    }

    /**
     * Getter of an author of given review
     * @param $reviewID ID of the review
     * @return mixed username of review's author
     */
    public function getReviewAuthor($reviewID){
        $reviewAuthorQuery = $this->connection->prepare("SELECT ".USER_NAME_COLUMN." FROM ".USER_TABLE." 
                                                      JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".USER_REVIEWS_ARTICLE_USER_FK_COLUMN." = ".USER_ID_COLUMN." 
                                                      JOIN ".REVIEW_TABLE." ON ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." = ".REVIEW_ID_COLUMN." 
                                                      WHERE ".REVIEW_ID_COLUMN." = :reviewID");
        $reviewAuthorQuery->bindValue(':reviewID', $reviewID);
        $reviewAuthorQuery->execute();
        return $reviewAuthorQuery->fetch()[USER_NAME_COLUMN];
    }

    /**
     * Getter og an article that is reviewed by given review
     * @param $reviewID review ID
     * @return mixed article reviewed by given review
     */
    public function getArticlesReviewedByReview($reviewID){
        $articleQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_ID_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".USER_NAME_COLUMN." FROM ".ARTICLE_TABLE." 
                                                 JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." 
                                                 JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_TABLE.".".ARTICLE_USER_FK_COLUMN." 
                                                 WHERE ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." = :reviewID");
        $articleQuery->bindValue(':reviewID', $reviewID);
        $articleQuery->execute();
        return $articleQuery->fetch();
    }

    /**
     * Creates new review in DB
     * @param $result result of a review (0-refuse, 1-accept)
     * @param $expertise expertise review
     * @param $language language review
     * @param $originality originality of an article review
     * @param $comment comment to the article
     * @return string ID of the new review
     */
    public function insertReview($result, $expertise, $language, $originality, $comment){
        $insertReviewQuery = $this->connection->prepare("INSERT INTO ".REVIEW_TABLE." (".REVIEW_ID_COLUMN.", ".REVIEW_RESULT_COLUMN.", ".REVIEW_EXPERTISE_COLUMN.", ".REVIEW_LANGUAGE_COLUMN.", ".REVIEW_ORIGINALITY_COLUMN.", ".REVIEW_COMMENT_COLUMN.", ".REVIEW_DATE_COLUMN.") 
                                                      VALUES (NULL, :result, :expertise, :languageReview, :originality, :comment, CURRENT_DATE());");
        $insertReviewQuery->bindValue(':result', $result);
        $insertReviewQuery->bindValue(':expertise', $expertise);
        $insertReviewQuery->bindValue(':languageReview', $language);
        $insertReviewQuery->bindValue(':originality', $originality);
        $insertReviewQuery->bindValue(':comment', $comment);
        $insertReviewQuery->execute();
        return $this->connection->lastInsertId();
    }

    /**
     * Assigns review to existing row in USER_REVIEWS_ARTICLE_TABLE
     * @param $reviewID ID of review to be assigned
     * @param $userID ID of reviewer
     * @param $articleID ID of reviewed article
     */
    public function updateArticleReview($reviewID, $userID, $articleID){
        $updateReviewLinkQuery = $this->connection->prepare("UPDATE ".USER_REVIEWS_ARTICLE_TABLE." SET ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." = :reviewID 
                                                            WHERE ".USER_REVIEWS_ARTICLE_USER_FK_COLUMN." = :userID AND ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." = :articleID");
        $updateReviewLinkQuery->bindValue('reviewID', $reviewID);
        $updateReviewLinkQuery->bindValue('userID', $userID);
        $updateReviewLinkQuery->bindValue('articleID', $articleID);
        $updateReviewLinkQuery->execute();
    }

    /**
     * Getter of unfinished reviews of given article
     * @param $articleID ID of article
     * @return array unfinished reviews of an article
     */
    public function getUnfinishedArticleReviews($articleID){
        $assignedReviewsQuery = $this->connection->prepare("SELECT ".USER_NAME_COLUMN.", ".USER_ID_COLUMN." FROM ".ARTICLE_TABLE." 
                                                         JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." 
                                                         JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_TABLE.".".USER_REVIEWS_ARTICLE_USER_FK_COLUMN."  
                                                         WHERE ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." = :articleID AND ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." IS NULL");
        $assignedReviewsQuery->bindValue(':articleID', $articleID);
        $assignedReviewsQuery->execute();
        return $assignedReviewsQuery->fetchAll();
    }

    /**
     * Assigns a review of given article to given user
     * @param $userID ID of reviewer
     * @param $articleID ID of reviewed article
     * @return bool if assignment was successful (review was not assigned already)
     */
    public function assignReview($userID, $articleID){
        try {
            $assignReviewQuery = $this->connection->prepare("INSERT INTO " . USER_REVIEWS_ARTICLE_TABLE . " (" . USER_REVIEWS_ARTICLE_USER_FK_COLUMN . ", " . USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN . ") VALUES (:userID, :articleID)");
            $assignReviewQuery->bindValue(':userID', $userID);
            $assignReviewQuery->bindValue('articleID', $articleID);
            $assignReviewQuery->execute();
            return true;
        }
        catch (PDOException $e){
            return false;
        }
    }

    /**
     * Deletes review assignment
     * @param $userID ID of reviewer
     * @param $articleID ID of reviewed article
     */
    public function deleteAssignedReview($userID, $articleID){
        $delete = $this->connection->prepare("DELETE FROM ".USER_REVIEWS_ARTICLE_TABLE." WHERE ".USER_REVIEWS_ARTICLE_USER_FK_COLUMN." = :userID AND ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." = :articleID");
        $delete->bindValue(':userID', $userID);
        $delete->bindValue(':articleID', $articleID);
        $delete->execute();
    }
}