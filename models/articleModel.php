<?php

/**
 * Class articleModel is used for getting article information from database
 */
class articleModel extends baseModel
{

    /**
     * Getter of number of total published articles
     * @return mixed number of published articles
     */
    public function getTotalNumberOfPublishedArticles(){
        $articlesNumberQuery = $this->connection->prepare("SELECT COUNT(*) as total FROM ".ARTICLE_TABLE."  
                                                            JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_USER_FK_COLUMN." 
                                                            WHERE ".ARTICLE_PUBLISHED_COLUMN." = 1");
        $articlesNumberQuery->execute();
        return $articlesNumberQuery->fetch()["total"];
    }

    /**
     * Getter of total number of published articles in given category
     * @param $categoryID id of category where to search for published articles
     * @return mixed number of published articles in given category
     */
    public function getTotalNumberOfPublishedArticlesInCategory($categoryID){
        $articlesNumberQuery = $this->connection->prepare("SELECT COUNT(*) as total FROM ".ARTICLE_TABLE."  
                                                            JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_USER_FK_COLUMN." 
                                                            JOIN ".CATEGORY_HAS_ARTICLE_TABLE." ON ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN." = ".ARTICLE_ID_COLUMN." 
                                                            WHERE ".ARTICLE_PUBLISHED_COLUMN." = 1 AND ".CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN." = :categoryID");
        $articlesNumberQuery->bindValue(':categoryID', $categoryID);
        $articlesNumberQuery->execute();
        return $articlesNumberQuery->fetch()["total"];
    }

    /**
     * Getter of all article overview in given category. Is designed to select only some articles to be able to work with pagination
     * @param $startArticle offset of first selected article in DB
     * @param $numberOfArticles number of articles to be selected
     * @param $categoryID id of category to search in
     * @return array articles according to given criteria
     */
    public function getArticlesByCategory($startArticle, $numberOfArticles, $categoryID){
        $articlesQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".USER_NAME_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".ARTICLE_OVERVIEW_COLUMN.", ".ARTICLE_ID_COLUMN." FROM ".ARTICLE_TABLE." 
                                              JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_USER_FK_COLUMN." 
                                              JOIN ".CATEGORY_HAS_ARTICLE_TABLE." ON ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN." = ".ARTICLE_ID_COLUMN." 
                                              WHERE ".ARTICLE_PUBLISHED_COLUMN." = 1 AND ".CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN." = :categoryID ORDER BY ".ARTICLE_DATE_COLUMN." DESC
                                              LIMIT ".$startArticle.", ".$numberOfArticles."");
        $articlesQuery->bindValue(':categoryID', intval($categoryID));
        $articlesQuery->execute();
        return $articlesQuery->fetchAll();
    }

    /**
     * Getter of all article overviews. Is designed to select only some articles to be able to work with pagination
     * @param $startArticle offset of first selected article in DB
     * @param $numberOfArticles number of articles to be selected
     * @return array articles according to given criteria
     */
    public function getArticles($startArticle, $numberOfArticles){
        $articlesQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".USER_NAME_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".ARTICLE_ID_COLUMN.", ".ARTICLE_OVERVIEW_COLUMN." FROM ".ARTICLE_TABLE." 
                                              JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_USER_FK_COLUMN." WHERE ".ARTICLE_PUBLISHED_COLUMN." = 1 ORDER BY ".ARTICLE_DATE_COLUMN." DESC
                                              LIMIT ".$startArticle.",".$numberOfArticles."");
        $articlesQuery->execute();
        return $articlesQuery->fetchAll();
    }

    /**
     * Selects a categories in which the given article is
     * @param $articleID ID of article
     * @return array names of categories of given article
     */
    public function getArticleCategories($articleID){
        $categoriesQuery = $this->connection->prepare("SELECT ".CATEGORY_TABLE.".* FROM ".CATEGORY_TABLE." 
                                                    JOIN ".CATEGORY_HAS_ARTICLE_TABLE." ON ".CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN." = ".CATEGORY_ID_COLUMN."
                                                    WHERE ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN." = :articleID");
        $categoriesQuery->bindValue(':articleID', $articleID);
        $categoriesQuery->execute();
        return $categoriesQuery->fetchAll();
    }

    /**
     * Getter of an article with given article ID
     * @param $id ID of an article
     * @return mixed selected article
     */
    public function getArticleByID($id){
        $articleQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".ARTICLE_OVERVIEW_COLUMN.", ".ARTICLE_TEXT_COLUMN.", ".ARTICLE_USER_FK_COLUMN.", ".USER_NAME_COLUMN.", ".ARTICLE_FILE.", ".ARTICLE_FILE." FROM ".ARTICLE_TABLE." 
                                             JOIN ".USER_TABLE." ON ".USER_ID_COLUMN." = ".ARTICLE_USER_FK_COLUMN." WHERE ".ARTICLE_ID_COLUMN." = :id");
        $articleQuery->bindValue(':id', $id);
        $articleQuery->execute();
        return $articleQuery->fetch();
    }

    /**
     * Getter of total number of published articles by given user
     * @param $userID ID of user
     * @return mixed total number of published articles by given user
     */
    public function getNumberOfUserPublishedArticles($userID){
        $articleCountQuery = $this->connection->prepare("SELECT COUNT(*) AS total FROM ".ARTICLE_TABLE." WHERE ".ARTICLE_USER_FK_COLUMN." = :userID 
                                                  AND ".ARTICLE_PUBLISHED_COLUMN." = 1");
        $articleCountQuery->bindValue(':userID', $userID);
        $articleCountQuery->execute();
        return $articleCountQuery->fetch();
    }

    /**
     * Selects all articles created by given user
     * @param $username name of the user
     * @return array all users articles
     */
    public function getUsersArticles($username){
        $articlesQuery = $this->connection->prepare("SELECT ".ARTICLE_ID_COLUMN.", ".ARTICLE_DATE_COLUMN." , ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_PUBLISHED_COLUMN." FROM ".ARTICLE_TABLE." 
                                          JOIN ".USER_TABLE." ON ".ARTICLE_USER_FK_COLUMN." = ".USER_ID_COLUMN." 
                                          WHERE ".USER_NAME_COLUMN." = :username ORDER BY ".ARTICLE_DATE_COLUMN." DESC");
        $articlesQuery->bindValue(':username',$username);
        $articlesQuery->execute();
        return $articlesQuery->fetchAll();
    }

    /**
     * Creates new article in DB
     * @param $title article title
     * @param $text article text
     * @param $description article description
     * @param $userID id of an author
     * @return string ID of the new article
     */
    public function insertArticle($title, $text, $description, $userID){
        $insertArticleQuery = $this->connection->prepare("INSERT INTO ".ARTICLE_TABLE." (".ARTICLE_ID_COLUMN.", ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_TEXT_COLUMN.", ".ARTICLE_PUBLISHED_COLUMN .", ".ARTICLE_OVERVIEW_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".ARTICLE_USER_FK_COLUMN.") 
                                                                    VALUES (NULL, :title, :text, b'0', :description, CURRENT_DATE(), :userID);");
        $insertArticleQuery->bindValue(':title', $title);
        $insertArticleQuery->bindValue(':text', $text);
        $insertArticleQuery->bindValue(':description', $description);
        $insertArticleQuery->bindValue(':userID', $userID);
        $insertArticleQuery->execute();
        return $this->connection->lastInsertId();
    }

    /**
     * Getter of articles that will be reviewed by given user
     * @param $userID user ID of selected reviewer
     * @return array selected articles
     */
    public function getArticlesToReview($userID){
        $toReviewQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_ID_COLUMN.", ".ARTICLE_TABLE.".".ARTICLE_USER_FK_COLUMN.", ".ARTICLE_DATE_COLUMN." FROM ".ARTICLE_TABLE." 
                                              JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." 
                                              WHERE ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." IS NULL AND ".USER_REVIEWS_ARTICLE_TABLE.".".USER_REVIEWS_ARTICLE_USER_FK_COLUMN." = :userID");
        $toReviewQuery->bindValue(':userID', $userID);
        $toReviewQuery->execute();
        return $toReviewQuery->fetchAll();
    }

    /**
     * Gets all articles that were reviewed by given user
     * @param $userID id of a reviewer
     * @return array selected articles
     */
    public function getReviewedArticles($userID){
        $finishedReviewsQuery = $this->connection->prepare("SELECT ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_TABLE.".".ARTICLE_USER_FK_COLUMN.", ".ARTICLE_TABLE.".".ARTICLE_DATE_COLUMN.", ".ARTICLE_ID_COLUMN.", ".REVIEW_EXPERTISE_COLUMN.", ".REVIEW_LANGUAGE_COLUMN.", ".REVIEW_ORIGINALITY_COLUMN.", ".REVIEW_RESULT_COLUMN.", ".REVIEW_ID_COLUMN." FROM ".REVIEW_TABLE." 
                                                     JOIN ".USER_REVIEWS_ARTICLE_TABLE." ON ".USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN." = ".REVIEW_ID_COLUMN." 
                                                     JOIN ".ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN." 
                                                     WHERE ".USER_REVIEWS_ARTICLE_TABLE.".".USER_REVIEWS_ARTICLE_USER_FK_COLUMN." = :userID");
        $finishedReviewsQuery->bindValue(':userID', $userID);
        $finishedReviewsQuery->execute();
        return $finishedReviewsQuery->fetchAll();
    }

    /**
     * Selects all articles that are in review now
     * @return mixed all articles in review
     */
    public function getArticlesInReview(){
        $where = array("column"=>ARTICLE_PUBLISHED_COLUMN, "value"=>0, "symbol"=>"=");
        return $this->DBSelectAll(ARTICLE_TABLE, "".ARTICLE_ID_COLUMN.", ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_USER_FK_COLUMN.", ".ARTICLE_DATE_COLUMN, array($where), "", array(array("column"=>ARTICLE_DATE_COLUMN, "sort"=>"DESC")));
    }

    /**
     * Selects all articles that were already reviewed and the decision if they are published or refused is known
     * @return array selected articles
     */
    public function getAllArticlesWithDecision(){
        $publishedArticlesQuery = $this->connection->prepare("SELECT ".ARTICLE_ID_COLUMN.", ".ARTICLE_TITLE_COLUMN.", ".ARTICLE_USER_FK_COLUMN.", ".ARTICLE_DATE_COLUMN.", ".ARTICLE_PUBLISHED_COLUMN." FROM ".ARTICLE_TABLE."
                                                       WHERE ".ARTICLE_PUBLISHED_COLUMN." = 1 OR ".ARTICLE_PUBLISHED_COLUMN." = 2 ORDER BY ".ARTICLE_DATE_COLUMN." DESC");
        $publishedArticlesQuery->execute();
        return $publishedArticlesQuery->fetchAll();
    }

    /**
     * Updates the publish flag of a given article
     * @param $state new publish state
     * @param $articleID article to be updated
     */
    public function updateArticlePublishState($state, $articleID){
        $publishArticleQuery = $this->connection->prepare("UPDATE ".ARTICLE_TABLE." SET ".ARTICLE_PUBLISHED_COLUMN." = :state WHERE ".ARTICLE_ID_COLUMN." = :articleID;");
        $publishArticleQuery->bindValue(':state', $state);
        $publishArticleQuery->bindValue(':articleID', $articleID);
        $publishArticleQuery->execute();
    }

    /**
     * Deletes article and all connected rows in DB
     * @param $articleID article to be deleted
     * @param $articleReviews reviewes of given article
     */
    public function deleteArticle($articleID, $articleReviews){
        //Rows that assingnes reviewers to do review of the article which will be deleted must be removed
        $where1 = array("column"=>USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN, "symbol"=>"=", "value"=>$articleID);
        $this->DBDelete(USER_REVIEWS_ARTICLE_TABLE, array($where1), "");

        //Then we need to delete all rows from the table which associates article with some category
        $where2 = array("column"=>CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN, "symbol"=>"=", "value"=>$articleID);
        $this->DBDelete(CATEGORY_HAS_ARTICLE_TABLE, array($where2), "");

        //And then the article is deleted
        $where3 = array("column"=>ARTICLE_ID_COLUMN, "symbol"=>"=", "value"=>$articleID);
        $this->DBDelete(ARTICLE_TABLE, array($where3), "");

        //Finaly all reviews that reviewed the article are deleted
        foreach ($articleReviews as $articleReview){
            $reviewID = $articleReview[REVIEW_ID_COLUMN];
            $where4 = array("column"=>REVIEW_ID_COLUMN, "symbol"=>"=", "value"=>$reviewID);
            $this->DBDelete(REVIEW_TABLE, array($where4), "");
        }
    }

    /**
     * Attachs a file to article
     * @param $articleID article ID to which the file will be attached
     * @param $fileName name of the attached file
     */
    public function addFileToArticle($articleID, $fileName){
        $addFile = $this->connection->prepare("UPDATE ".ARTICLE_TABLE." SET ".ARTICLE_FILE." = :filename WHERE ".ARTICLE_ID_COLUMN." = :articleID");
        $addFile->bindValue(':filename', $fileName);
        $addFile->bindValue('articleID', $articleID);
        $addFile->execute();
    }

    public function updateArticle($articleID, $title, $overview, $text){
        $updateQuery = $this->connection->prepare("UPDATE ".ARTICLE_TABLE." SET ".ARTICLE_TITLE_COLUMN." = :title , ".ARTICLE_OVERVIEW_COLUMN." = :overview , ".ARTICLE_TEXT_COLUMN." = :text WHERE ".ARTICLE_ID_COLUMN." = :articleID ");
        $updateQuery->bindValue(':title',$title);
        $updateQuery->bindValue(':overview',$overview);
        $updateQuery->bindValue(':text',$text);
        $updateQuery->bindValue('articleID', $articleID);
        $updateQuery->execute();
    }

    public function updateArticleWithPDF($articleID, $title, $overview, $text, $file){
        $updateQuery = $this->connection->prepare("UPDATE ".ARTICLE_TABLE." SET ".ARTICLE_TITLE_COLUMN." = :title , ".ARTICLE_OVERVIEW_COLUMN." = :overview , ".ARTICLE_TEXT_COLUMN." = :text , ".ARTICLE_FILE." = :file WHERE ".ARTICLE_ID_COLUMN." = :articleID ");
        $updateQuery->bindValue(':title',$title);
        $updateQuery->bindValue(':overview',$overview);
        $updateQuery->bindValue(':text',$text);
        $updateQuery->bindValue(':file',$file);
        $updateQuery->bindValue('articleID', $articleID);
        $updateQuery->execute();
    }

}