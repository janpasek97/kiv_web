<?php

/**
 * Class userModel is used for getting user information
 */
class userModel extends baseModel
{
    /**
     * Gets all user with given user right (eg. "author")
     * @param $right user right (eg. "author")
     * @return array all users with given user right
     */
    public function getAllUsersWithRight($right){
        $allAuthorsQuery = $this->connection->prepare("SELECT ".USER_ID_COLUMN.", ".USER_NAME_COLUMN." FROM ".USER_TABLE."
                                            JOIN ".USER_HAS_ROLE_TABLE." ON ".USER_HAS_RIGHT_USER_FK_COLUMN." = ".USER_ID_COLUMN."
                                            JOIN ".RIGHT_TABLE." ON ".USER_HAS_RIGHT_RIGHT_FK_COLUMN." = ".RIGHT_ID_COLUMN."
                                            WHERE ".RIGHT_NAME_COLUMN."= :right");
        $allAuthorsQuery->bindValue(':right', $right);
        $allAuthorsQuery->execute();
        return $allAuthorsQuery->fetchAll();
    }

    /**
     * Getter of all categories in which the given author created some article
     * @param $userID ID of a user
     * @return array selected categories
     */
    public function getUserCategories($userID){
        $authorCategoriesQuery = $this->connection->prepare("SELECT ".CATEGORY_NAME_COLUMN." FROM ".CATEGORY_TABLE." 
                                                      JOIN ".CATEGORY_HAS_ARTICLE_TABLE." ON ".CATEGORY_ID_COLUMN." = ".CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN." 
                                                      JOIN ".ARTICLE_TABLE." ON ".ARTICLE_ID_COLUMN." = ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN." 
                                                      WHERE ".ARTICLE_USER_FK_COLUMN." = :userID 
                                                      GROUP BY ".CATEGORY_NAME_COLUMN);
        $authorCategoriesQuery->bindValue(':userID', $userID);
        $authorCategoriesQuery->execute();
        return $authorCategoriesQuery->fetchAll();
    }

    /**
     * Getter of user ID of given username
     * @param $username username of user
     * @return mixed ID of user
     */
    public function getUserID($username){
        $where = array();
        $where["column"] = USER_NAME_COLUMN;
        $where["symbol"] = "=";
        $where["value"] = $username;
        return $this->DBSelectOne(USER_TABLE, USER_ID_COLUMN, array($where))[USER_ID_COLUMN];
    }

    /**
     * Getter of username of given user by user ID
     * @param $userID
     * @return mixed username of given user
     */
    public function getUsername($userID){
        $where = array();
        $where["column"] = USER_ID_COLUMN;
        $where["symbol"] = "=";
        $where["value"] = $userID;
        return $this->DBSelectOne(USER_TABLE, USER_NAME_COLUMN, array($where))[USER_NAME_COLUMN];
    }

    /**
     * Getter of all registered users
     * @return mixed all users
     */
    public function getAllUsers(){
        $columns = "".USER_NAME_COLUMN.", ".USER_EMAIL_COLUMN.", ".USER_ID_COLUMN;
        return $this->DBSelectAll(USER_TABLE, $columns, array());
    }

    /**
     * Getter of given user's rights
     * @param $userID user ID
     * @return array user's rights
     */
    public function getUsersRights($userID){
        $rightsQuery = $this->connection->prepare("SELECT ".RIGHT_NAME_COLUMN." FROM ".RIGHT_TABLE." 
                                            JOIN ".USER_HAS_ROLE_TABLE." ON ".USER_HAS_RIGHT_RIGHT_FK_COLUMN."=".RIGHT_ID_COLUMN." 
                                            WHERE ".USER_HAS_RIGHT_USER_FK_COLUMN." = :userID");
        $rightsQuery->bindValue(':userID', $userID);
        $rightsQuery->execute();
        return $rightsQuery->fetchAll();
    }
}