<?php

/**
 * Class UserLogin is used for handling user login session
 */
class UserLogin extends baseModel
{

    /** @var Session session which stores information about login */
    private $session;
    /** @var string name of the session which has an information about user's name */
    private $sName = "name";
    /** @var string name of the session which has an information the date when user logged in */
    private $sDate = "date";

    /**
     * UserLogin constructor
     */
    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Checks if a user is logged in
     * @return bool true if user is logged in
     */
    public function isUserLogged(){
        return $this->session->isSessionSet($this->sName);
    }

    /**
     * Logs the user in after checking login details with database
     * @param $username name of a user that want's to log in
     * @param $password password of a user that want's to log in
     * @return bool is the login was successful
     */
    public function login($username, $password) {
        $passwordQuery = $this->connection->prepare("SELECT uzivatel.heslo FROM uzivatel WHERE uzivatel.uzivatelske_jmeno = ?");
        $passwordQuery->execute(array($username));
        $result = $passwordQuery->fetchAll();
        if($passwordQuery->rowCount() > 0) {
            if (password_verify($password, $result[0]["heslo"])) {
                $this->session->addSession($this->sName, $username);
                $this->session->addSession($this->sDate, date("d. m. Y, G:m:s"));
                return true;
            } else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     * Loggs out the user
     */
    public function logout(){
        $this->session->removeSession($this->sName);
        $this->session->removeSession($this->sDate);
    }

    /**
     * Creates new user in database with author right and
     * checks if a user with given username and email already exists
     * @param $username username of the new user
     * @param $email email of the new user
     * @param $password password of the new user
     * @return bool true if registration was successful
     */
    public function signup($username, $email, $password){
        $existingUserQuery = $this->connection->prepare("SELECT uzivatel.uzivatelske_jmeno, uzivatel.email FROM uzivatel WHERE uzivatel.uzivatelske_jmeno = ? OR uzivatel.email = ?");
        $existingUserQuery->execute(array($username, $email));
        if($existingUserQuery->rowCount() == 0){
            $authorIDQuery = $this->connection->prepare("SELECT role.cislo_role FROM role WHERE role.nazev_role = ?");
            $authorIDQuery->execute(array("author"));
            $authorIDResult = $authorIDQuery->fetchAll();
            $authorID = $authorIDResult[0]["cislo_role"];
            $createUserQuery = $this->connection->prepare("INSERT INTO uzivatel (uzivatel.uzivatelske_jmeno, uzivatel.email, uzivatel.heslo) VALUES (?, ?, ?)");
            $createUserQuery->execute(array($username, $email, $password));
            $newUserIDQuery = $this->connection->prepare("SELECT uzivatel.cislo_uzivatele FROM uzivatel WHERE uzivatel.uzivatelske_jmeno=?");
            $newUserIDQuery->execute(array($username));
            $newUserIDResult = $newUserIDQuery->fetchAll();
            $newUserID = $newUserIDResult[0]["cislo_uzivatele"];
            $setAuthorQuery = $this->connection->prepare("INSERT INTO uzivatel_ma_roli (ROLE_cislo_role, UZIVATEL_cislo_uzivatele) VALUES (?, ?)");
            $setAuthorQuery->execute(array($authorID, $newUserID));
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Getter of logged user information
     * @return array with username of logged user
     */
    public function getLoggedUserInformation(){
        $username = $this->session->readSession($this->sName);
        return array("username"=>$username);
    }

    /**
     * Checks if logged user has a given right (author, reviewer, administrator)
     * @param $right user right to be checked
     * @return bool true if user has a given user right
     */
    public function hasUserRight($right){
        $isAdminQuery = $this->connection->prepare("SELECT role.nazev_role FROM role 
                                            JOIN uzivatel_ma_roli ON role.cislo_role = uzivatel_ma_roli.ROLE_cislo_role 
                                            JOIN uzivatel ON uzivatel.cislo_uzivatele = uzivatel_ma_roli.UZIVATEL_cislo_uzivatele 
                                            WHERE uzivatel.uzivatelske_jmeno = ? AND role.nazev_role = ?");
        $username = $this->session->readSession($this->sName);
        $isAdminQuery->execute(array($username, $right));
        $resultsNr = $isAdminQuery->rowCount();

        if($resultsNr > 0){
            return true;
        }
        else {
            return false;
        }
    }
}