<?php

/**
 * Class userRightModel is used or handling DB requests from USERR_RIGHT_TABLE
 */
class userRightModel extends baseModel {

    /**
     * Deletes all rights of a user
     * @param $userID user which rights will be deleted
     */
    public function deleteAllUsersRights($userID){
        $deleteUserRights = $this->connection->prepare("DELETE FROM ".USER_HAS_ROLE_TABLE." WHERE ".USER_HAS_RIGHT_USER_FK_COLUMN." = :userID");
        $deleteUserRights->bindValue(':userID', $userID);
        $deleteUserRights->execute();
    }

    /**
     * Gets an ID of given user right
     * @param $right user right name
     * @return mixed ID of user right
     */
    public function getRightID($right){
        $where = array("column"=>RIGHT_NAME_COLUMN, "symbol"=>"=", "value"=>$right);
        return $this->DBSelectOne(RIGHT_TABLE, RIGHT_ID_COLUMN, array($where))[RIGHT_ID_COLUMN];
    }

    /**
     * Assigns a right with given ID to a given user
     * @param $userID ID of a user
     * @param $rightID ID of a user right
     */
    public function assignRight($userID, $rightID){
        $insertRightQuery = $this->connection->prepare("INSERT INTO ".USER_HAS_ROLE_TABLE." (".USER_HAS_RIGHT_RIGHT_FK_COLUMN.", ".USER_HAS_RIGHT_USER_FK_COLUMN.") VALUES (:rightID, :userID)");
        $insertRightQuery->bindValue(':userID', $userID);
        $insertRightQuery->bindValue(':rightID', $rightID);
        $insertRightQuery->execute();
    }
}
