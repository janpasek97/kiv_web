<?php

/**
 * Class categoryModel is used to handle requests connected with category table
 */
class categoryModel extends baseModel
{
    /**
     * Getter of all categories
     * @return mixed all categories in DB
     */
    public function getAllCategories(){
        return $this->DBSelectAll(CATEGORY_TABLE, "*", array());
    }

    /**
     * Selects category name according to given category ID
     * @param $id ID of category
     * @return mixed name of given category
     */
    public function getCategoryNameByID($id){
        $where[] = array("column"=>CATEGORY_ID_COLUMN, "symbol"=>"=", "value"=>$id);
        return $this->DBSelectOne(CATEGORY_TABLE, CATEGORY_NAME_COLUMN, $where)[CATEGORY_NAME_COLUMN];
    }


    /**
     * Assignes an article to selected categories
     * @param $categories categories array with ID of category to which the article will be assigned
     * @param $articleID ID of article
     */
    public function assignCategoriesToArticle($categories, $articleID){
        $categoryIDQuery = $this->connection->prepare("SELECT ".CATEGORY_ID_COLUMN." FROM ".CATEGORY_TABLE." WHERE ".CATEGORY_NAME_COLUMN." = :categoryName");
        $assignCategoryQuery = $this->connection->prepare("INSERT INTO ".CATEGORY_HAS_ARTICLE_TABLE." (".CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN.", ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN.") 
                                                                     VALUES (:categoryID, :articleID)");
        foreach ($categories as $category){
            $categoryIDQuery->bindValue(':categoryName', $category);
            $categoryIDQuery->execute();
            $categoryID = $categoryIDQuery->fetch()[CATEGORY_ID_COLUMN];

            $assignCategoryQuery->bindValue(':categoryID', $categoryID);
            $assignCategoryQuery->bindValue('articleID', $articleID);
            $assignCategoryQuery->execute();
        }
    }

    public function unassignAllCategories($articleID){
        $unassignQuery = $this->connection->prepare("DELETE FROM ".CATEGORY_HAS_ARTICLE_TABLE." WHERE ".CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN." = :articleID");
        $unassignQuery->bindValue(':articleID', $articleID);
        $unassignQuery->execute();
    }
}