<?php
    //DB CONNECTION DETAILS
    define('SQL_HOST', 'localhost');
    define('SQL_DBNAME', 'ITConf');
    define('SQL_USERNAME','janpasek97');
    define('SQL_PASSWORD', 'Martin.2002');

    //DB TABLES CONFIG
    define('ARTICLE_TABLE','clanek');
    define('REVIEW_TABLE','recenze');
    define('RIGHT_TABLE','role');
    define('CATEGORY_TABLE','rubrika');
    define('CATEGORY_HAS_ARTICLE_TABLE','rubrika_ma_clanek');
    define('USER_TABLE','uzivatel');
    define('USER_HAS_ROLE_TABLE','uzivatel_ma_roli');
    define('USER_REVIEWS_ARTICLE_TABLE','uzivatel_recenzuje_clanek');

    //ARTICLE TABLE COLUMNS
    define('ARTICLE_ID_COLUMN','cislo_clanku');
    define('ARTICLE_TITLE_COLUMN','titulek');
    define('ARTICLE_TEXT_COLUMN','text_clanku');
    define('ARTICLE_PUBLISHED_COLUMN','publikovany');
    define('ARTICLE_OVERVIEW_COLUMN','uvod_clanku');
    define('ARTICLE_DATE_COLUMN','datum');
    define('ARTICLE_USER_FK_COLUMN','UZIVATEL_cislo_uzivatele');
    define('ARTICLE_FILE','pdfSoubor');

    //REVIEW TABLE COLUMNS
    define('REVIEW_ID_COLUMN','cislo_recenze');
    define('REVIEW_RESULT_COLUMN','konecne_rozhodnuti');
    define('REVIEW_EXPERTISE_COLUMN','hodnoceni_odborny_prinos');
    define('REVIEW_LANGUAGE_COLUMN','hodnoceni_jazyk_stylistika');
    define('REVIEW_ORIGINALITY_COLUMN','hodnoceni_originalita_tematu');
    define('REVIEW_COMMENT_COLUMN','komentar');
    define('REVIEW_DATE_COLUMN','datum');

    //USER RIGHT TABLE COLUMNS
    define('RIGHT_ID_COLUMN','cislo_role');
    define('RIGHT_NAME_COLUMN','nazev_role');

    //CATEGORY TABLE COLUMNS
    define('CATEGORY_ID_COLUMN','cislo_rubriky');
    define('CATEGORY_NAME_COLUMN','nazev_rubriky');

    //CATEGORY HAS ARTICLE TABLE COLUMNS
    define('CATEGORY_HAS_ARTICLE_CATEGORY_FK_COLUMN','RUBRIKA_cislo_rubriky');
    define('CATEGORY_HAS_ARTICLE_ARTICLE_FK_COLUMN','CLANEK_cislo_clanku');

    //USER TABLE COLUMNS
    define('USER_ID_COLUMN','cislo_uzivatele');
    define('USER_NAME_COLUMN','uzivatelske_jmeno');
    define('USER_EMAIL_COLUMN','email');
    define('USER_PASSWORD_COLUMN','heslo');

    //USER HAS RIGHT TABLE COLUMNS
    define('USER_HAS_RIGHT_USER_FK_COLUMN','UZIVATEL_cislo_uzivatele');
    define('USER_HAS_RIGHT_RIGHT_FK_COLUMN','ROLE_cislo_role');

    //USER REVIEWS ARTICLE TABLE COLUMN
    define('USER_REVIEWS_ARTICLE_USER_FK_COLUMN','UZIVATEL_cislo_uzivatele');
    define('USER_REVIEWS_ARTICLE_ARTICLE_FK_COLUMN','CLANEK_cislo_clanku');
    define('USER_REVIEWS_ARTICLE_REVIEW_FK_COLUMN','RECENZE_cislo_recenze');
