<?php

/**
 * Class Session is used for storing and reading cookies
 */
class Session {

    /**
     * Session constructor.
     * Starts the session
     */
    public function __construct()
    {
        session_start();
    }

    /**
     * Adds new information into the session
     * @param $name session name
     * @param $value session value
     */
    public function addSession($name, $value){
        $_SESSION[$name] = $value;
    }

    /**
     * Checks if the session with given name is set
     * @param $name name of the cookie to be set
     * @return bool true if session is set
     */
    public function isSessionSet($name){
        return isset($_SESSION[$name]);
    }

    /**
     * Reads a session with given name
     * @param $name name of the session to be read out
     * @return null
     */
    public function readSession($name){
        if($this->isSessionSet($name)){
            return $_SESSION[$name];
        }
        else {
            return null;
        }
    }

    /**
     * Removes the session with given name
     * @param $name name of the session to be removed
     */
    public function removeSession($name){
        unset($_SESSION[$name]);
    }
}